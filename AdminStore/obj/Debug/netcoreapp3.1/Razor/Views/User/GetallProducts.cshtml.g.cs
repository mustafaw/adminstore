#pragma checksum "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "188878e7f29d2682cefbbb109dd510827df0835b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_User_GetallProducts), @"mvc.1.0.view", @"/Views/User/GetallProducts.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\Projects\StoreAdmin\AdminStore\Views\_ViewImports.cshtml"
using AdminStore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
using AdminStore.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
using AdminStore.Services;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"188878e7f29d2682cefbbb109dd510827df0835b", @"/Views/User/GetallProducts.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"acd49c3ba04e2ec959afb221d0ea6a624543ce7f", @"/Views/_ViewImports.cshtml")]
    public class Views_User_GetallProducts : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<AdminStore.Models.TblProduct>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/NoImage.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("height:100px;width:100px"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 5 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
  
    Layout = null;
    PathClass obj = new PathClass();
    string path = obj.GetDomainNme();


#line default
#line hidden
#nullable disable
            WriteLiteral(@"<table class=""display vendor-table"" id=""basic-1"">
    <thead>
        <tr>
            <th>Date</th>
            <th>User Name</th>
            <th>Store Name</th>
            <th>Category</th>
            <th>Product Name</th>
            <th>Product Unit</th>
            <th>Total Quantity</th>
            <th>Available Quantity</th>
            <th>Is Front</th>
            <th>Thumbnails</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
");
#nullable restore
#line 28 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
         foreach (var item in Model)
        {
            StoreKingbhaiContext db = new StoreKingbhaiContext();
            var getuserDetail = db.TblUserDetail.FirstOrDefault(x => x.UserId == item.UserId);
            var getstore = db.TblStore.FirstOrDefault(x => x.Id == item.StoreId);
            var getcategory = db.TblCategory.FirstOrDefault(x => x.Id == item.CategoryId);
            var getunit = db.TblProductUnit.FirstOrDefault(x => x.Id == item.ProductUnitId);


#line default
#line hidden
#nullable disable
            WriteLiteral("        <tr>\r\n            <td>\r\n                ");
#nullable restore
#line 38 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
           Write(Convert.ToDateTime(item.CreatedOn).ToString("dd/MM/yyyy"));

#line default
#line hidden
#nullable disable
            WriteLiteral(";\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 41 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
           Write(getuserDetail.FullName);

#line default
#line hidden
#nullable disable
            WriteLiteral(" ( ");
#nullable restore
#line 41 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
                                     Write(getuserDetail.Cnic);

#line default
#line hidden
#nullable disable
            WriteLiteral(");\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 44 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
           Write(getstore.CompanyName);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 47 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
           Write(getcategory.CategoryName);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 50 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
           Write(item.ProductName);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 53 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
           Write(getunit.UnitName);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 56 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
           Write(item.TotalQuantity);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 59 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
           Write(item.AvailableQuantity);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n");
#nullable restore
#line 62 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
                 if (item.IsFront == true)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <span style=\"color:green\">Yes</span>\r\n");
#nullable restore
#line 65 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
                }
                else
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <span style=\"color:red\">No</span>\r\n");
#nullable restore
#line 69 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"

                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </td>\r\n            <td>\r\n");
#nullable restore
#line 73 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
                 if (item.ThumbNailImage != null && item.ThumbNailImage != "")
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <img");
            BeginWriteAttribute("src", " src=\"", 2296, "\"", 2327, 2);
#nullable restore
#line 75 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
WriteAttributeValue("", 2302, path, 2302, 5, false);

#line default
#line hidden
#nullable disable
#nullable restore
#line 75 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
WriteAttributeValue("", 2307, item.ThumbNailImage, 2307, 20, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" style=\"height:100px;width:100px;\" />\r\n");
#nullable restore
#line 76 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
                }
                else
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "188878e7f29d2682cefbbb109dd510827df0835b10009", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
#nullable restore
#line 80 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </td>\r\n            <td>\r\n                <div>\r\n                    <i class=\"fa fa-edit mr-2 font-success\" style=\"cursor:pointer\"");
            BeginWriteAttribute("onclick", " onclick=\"", 2670, "\"", 2696, 3);
            WriteAttributeValue("", 2680, "Edit(\'", 2680, 6, true);
#nullable restore
#line 84 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
WriteAttributeValue("", 2686, item.Id, 2686, 8, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 2694, "\')", 2694, 2, true);
            EndWriteAttribute();
            WriteLiteral("></i>\r\n                    <i class=\"fa fa-trash font-danger\" style=\"cursor:pointer\"");
            BeginWriteAttribute("onclick", " onclick=\"", 2781, "\"", 2809, 3);
            WriteAttributeValue("", 2791, "Delete(\'", 2791, 8, true);
#nullable restore
#line 85 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
WriteAttributeValue("", 2799, item.Id, 2799, 8, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 2807, "\')", 2807, 2, true);
            EndWriteAttribute();
            WriteLiteral("></i>\r\n                </div>\r\n            </td>\r\n        </tr>\r\n");
#nullable restore
#line 89 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallProducts.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n    </tbody>\r\n</table>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<AdminStore.Models.TblProduct>> Html { get; private set; }
    }
}
#pragma warning restore 1591
