﻿$(document).ready(function () {
    $("#DOB").datepicker();
})

async function SaveUserDetail() {
    var Id = $("#Id").val();
    var UserID = $("#UserID").val();
    var FullName = $("#FullName").val();
    var DOB = $("#DOB").val();
    var BankAccountTitle = $("#BankAccountTitle").val();
    var AccountNumber = $("#AccountNumber").val();
    var BranchCode = $("#BranchCode").val();
    var Country = $("#Country").val();
    var Region = $("#Region").val();
    var City = $("#City").val();
    var Area = $("#Area").val();
    var Address = $("#Address").val();
    var AboutUser = $("#AboutUser").val();
    var cnic = $("#cnic").val();
    var Image = await uploadProfileImage()
    if (Image == undefined || Image == "") {
        Image = $("#HideImgUrl").val()
    }
    var ChequeImage = await UploadChequeImage()
    if (ChequeImage == undefined || ChequeImage == "") {
        ChequeImage = $("#HideChequeImgUrl").val()
    }
    var SaveURL = $("#SaveURL").val();

    $.ajax({
        url: SaveURL,
        async: true,
        type: "POST",
        data: {
            Id: Id, UserID: UserID, FullName: FullName, BankAccountTitle: BankAccountTitle, DOB: DOB, AccountNumber: AccountNumber
            , BranchCode: BranchCode, Country: Country, cnic: cnic,
            Region: Region, City: City, Area: Area, Address: Address, AboutUser: AboutUser, Image: Image, ChequeImage: ChequeImage
        },
        success: function (res) {
            if (res == "true") {
                swal("Success", "User Detail Addedd Succeffuly", "success").then(function () {
                    location.reload();
                })
            }
            else if (res == "updated")
            {
                swal("Success", "User Detail Updated Succeffuly", "success").then(function () {
                    location.reload();
                })
            }
            else {
                swal("Error", "Somethig went wrong", "error")
            }
        }
    })

}
var GetUserDetail = function () {
    var URL = $("#GetUserDetailURL").val();
    var UserID = $("#UserID").val();
    if (UserID != "") {
        $.ajax({
            url: URL,
            async: true,
            type: "POST",
            data: {
                UserID: UserID
            },
            success: function (res) {
                console.log(res);
                $("#Id").val(res.res.id);
                //$("#UserID").val(res.res.userId).change();
                $("#FullName").val(res.res.fullName);
                $("#DOB").val(res.res.dob);
                $("#BankAccountTitle").val(res.res.accountTitle);
                $("#AccountNumber").val(res.res.accountNo);
                $("#BranchCode").val(res.res.branchCode);
                $("#Country").val(res.res.country).change();
                $("#RegionId").val(res.res.region);
                $("#CityId").val(res.res.city);
                $("#AreaId").val(res.res.area);
                $("#Address").val(res.res.addressDescription);
                $("#AboutUser").val(res.res.about);
                $("#cnic").val(res.res.cnic);
                $("#HideChequeImgUrl").val(res.res.chequeImage);
                $("#HideImgUrl").val(res.res.about);
                $(window).scrollTop(0);
            }
        })
    }
    else {
        clearFields();
    }
 
}

var clearFields = function () {
    $("#Id").val("0");
    //$("#UserID").val(res.res.userId).change();
    $("#FullName").val("");
    $("#DOB").val("");
    $("#BankAccountTitle").val("");
    $("#AccountNumber").val("");
    $("#BranchCode").val("");
    $("#Country").val("").change();
    $("#RegionId").val("0");
    $("#CityId").val("0");
    $("#AreaId").val("0");
    $("#Address").val("");
    $("#AboutUser").val("");
    $("#cnic").val("");
    $("#HideChequeImgUrl").val("");
    $("#HideImgUrl").val("");
}


///Save Profile Image
function uploadProfileImage() {
    var ImageName;
    var input = document.getElementById("ProfilePicture");
    var url = $("#SaveProfileImageURL").val();
    var files = input.files;
    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i != files.length; i++) {
            formData.append("files", files[i]);
        }
        $.ajax(
            {
                url: url,
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                type: "POST",
                success: function (data) {

                    ImageName = data
                }
            }
        );
    }


    return ImageName;
}



///Save Cheque Image
function UploadChequeImage() {
    var ImageName;
    var input = document.getElementById("ChequeImage");
    var url = $("#SaveChequeImageURL").val();
    var files = input.files;
    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i != files.length; i++) {
            formData.append("files", files[i]);
        }
        $.ajax(
            {
                url: url,
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                type: "POST",
                success: function (data) {

                    ImageName = data
                }
            }
        );
    }


    return ImageName;
}