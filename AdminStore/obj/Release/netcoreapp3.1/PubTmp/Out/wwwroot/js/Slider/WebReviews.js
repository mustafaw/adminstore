﻿var ShowNotFront = function (id) {
    var ShowNotFrontURL = $("#ShowNotFrontURL").val();

    $.ajax({
        url: ShowNotFrontURL,
        async: true,
        type: "POST",
        data: {
            Id: id
        },
        success: function (res) {
            if (res == "true") {
                swal("success", "Web Review Is Not Front successfully", "success").then(function () {
                    location.reload();
                });
            }
            else {
                swal("errror", res, "error");
            }
        }
    })
}
var ShowFront = function (id) {
    var ShowFrontURL = $("#ShowFrontURL").val();

    $.ajax({
        url: ShowFrontURL,
        async: true,
        type: "POST",
        data: {
            Id: id
        },
        success: function (res) {
            if (res == "true") {
                swal("success", "Web Review Is Front successfully", "success").then(function () {
                    location.reload();
                });
            }
            else {
                swal("errror",res, "error");
            }
        }
    })
}