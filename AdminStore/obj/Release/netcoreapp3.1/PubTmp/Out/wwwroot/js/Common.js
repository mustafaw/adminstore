﻿$(document).ready(function () {

    $('input').attr('autocomplete', 'off')
    //$("input").addClass("autocomplete")

})
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 86 || charCode != 118))
        return false;
    return true;
}


function IsNumeric(event) {
    var key = window.event ? event.keyCode : event.which;


    if (key === 46) {
        return true;
    }

    if (event.keyCode === 8 || event.keyCode === 46
        || event.keyCode === 37 || event.keyCode === 39 || event.keyCode === 9) {


        return true;
    }
    else if (key < 48 || key > 57) {
        return false;
    }
    else return true;
}

var CommonJs = function () {
    return {
        Shuffle: function (array) {
            var m = array.length, t, i;
            // While there remain elements to shuffle…
            while (m) {

                // Pick a remaining element…
                i = Math.floor(Math.random() * m--);

                // And swap it with the current element.
                t = array[m];
                array[m] = array[i];
                array[i] = t;
            }
            return array;
        },
        IsNumeric: function (event) {
            var key = event.keyCode || event.charCode; //window.event ? event.keyCode : event.which;
            if (key === 46) {
                return true;
            }
            if (event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 37 || event.keyCode === 39 || event.keyCode === 9) {
                return true;
            }
            else if (key < 48 || key > 57) {
                return false;
            }
            else return true;
        },
        IsAlphabetic: function (event) {
            var key = window.event ? event.keyCode : event.which;
            if (key === 46 || key === 32) {
                return true;
            }
            if (event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 37 || event.keyCode === 39 || event.keyCode === 9) {
                return true;
            }
            else if (key < 65 || key > 90) {
                if (key > 96 && key < 123) {
                    return true;
                } else {
                    return false;
                }
            }
            else return true;
        },
        IsPhoneNumber: function (event) {
            var key = window.event ? event.keyCode : event.which;
            if (key === 46 || key === 43) {
                return true;
            }
            if (event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 37 || event.keyCode === 39 || event.keyCode === 107 || event.keyCode === 9) {
                return true;
            }
            else if (key < 48 || key > 57) {
                return false;
            }
            else return true;
        },
        ValidateEmail: function (email) {
            var result = "";
            if (email !== "") {
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                if (!(filter.test(email)))
                    return false;
                else
                    return true;
            }
            else
                return false;
        },
        WordCount_KeyPress: function (event, elem, maxWords) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode === 8 || event.keyCode === 46
                || event.keyCode === 37 || event.keyCode === 39) {
                return true;
            } else {
                var wordcount = 0;
                wordcount = $(elem).val().split(/\b[\s,\.-:;]*/).length;
                if (wordcount > maxWords) {
                    return false;
                } else {
                    return true;
                }
            }
        },
        WordCount: function (value, maxWords) {
            var wordcount = 0;
            wordcount = value.split(/\b[\s,\.-:;]*/).length;
            if (wordcount > maxWords) {
                return false;
            } else {
                return true;
            }
        },
        WordCount_KeyUp: function (field, remainingId, maxWords) {
            var fieldLen = field.value.length;
            $('#' + remainingId).html(fieldLen + " Characters...");
            if (field.value.length > maxWords) {
                field.value = field.value.substring(0, maxWords);
                $('#' + remainingId).html(maxWords + " Characters...");
            }
        },
        Random_String: function (length) {
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
            var randomstring = "";
            for (var i = 0; i < length; i++) {
                var rnum = Math.floor(Math.random() * chars.length);
                randomstring += chars.substring(rnum, rnum + 1);
            }
            return randomstring;
        },
        Random_Integer: function (length) {
            var chars = "0123456789";
            var randomstring = "";
            for (var i = 0; i < length; i++) {
                var rnum = Math.floor(Math.random() * chars.length);
                randomstring += chars.substring(rnum, rnum + 1);
            }
            return randomstring;
        },

        Reverse_String: function (s) {
            return s.split("").reverse().join("");
        },
        Remove_Comas: function (input) {
            return input.replace(/\,/g, "");
        },
        Validate_Form: function (form_id) {

            var er = 0;
            $("#" + form_id + " .validate").each(function (index, elem) {
                var value = $(elem).val();
                if (value === "") {
                    $(elem).addClass("error-border");
                    er++;
                } else {
                    $(elem).removeClass("error-border");
                }
            });
            if (er > 0) {
                return false;
            }
            return true;
        },
        serializeFormToJson: function (formId, excludedNames) {
            var o = {};
            var a = $("#" + formId).serializeArray();
            var names = [];
            if (excludedNames !== undefined && excludedNames !== null && excludedNames !== "") {
                names = excludedNames.split(",");
            }
            $.each(a, function () {
                var match = false;
                var exist = $.inArray(this.name, names) //excludedNames.indexOf(this.name);
                if (exist !== -1) {
                    match = true;
                }
                if (match === false) {
                    if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                }
            });
            return o;
        },

    }
}();
