﻿var GetallRegionDRP = function () {
    var CountryId = $("#Country").val();
    var GetallRegionDRPURL = $("#GetallRegionDRPURL").val();
    $.ajax({
        url: GetallRegionDRPURL,
        type: "post",
        data: {Country: CountryId },
        success: function (res) {
            $("#Region").html(res);
            var Id = $("#RegionId").val();
            if (Id != "0") {
                $("#Region").val(Id).change();
            }
        }
    })
}
var GetallCityDRP = function () {
    var Region = $("#Region").val();
    var GetallRegionDRPURL = $("#GetallCityDRPURL").val();
    $.ajax({
        url: GetallRegionDRPURL,
        type: "post",
        data: {  Region: Region },
        success: function (res) {
            $("#City").html(res);
            var Id = $("#CityId").val();
            if (Id != "0") {
                $("#City").val(Id).change();
            }
        }
    })
}
var GetallAreaDRP = function () {
    var City = $("#City").val();
    var GetallAreaDRPURL = $("#GetallAreaDRPURL").val();
    $.ajax({
        url: GetallAreaDRPURL,
        type: "post",
        data: { City: City },
        success: function (res) {
            $("#Area").html(res);
            var Id = $("#AreaId").val();
            if (Id != "0") {
                $("#Area").val(Id).change();
            }

        }
    })
}