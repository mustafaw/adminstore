﻿$(document).ready(function () {
    Getall();
})

var Save = function () {
    var Id = $("#Id").val();
    var SecondCategory = $("#SecondCategory").val();
    var ThirdCategoryName = $("#ThirdCategoryName").val();
    var SaveURL = $("#SaveURL").val();
    if (SecondCategory != "" && ThirdCategoryName != "") {
        $.ajax({
            url: SaveURL,
            type: "post",
            data: { Id: Id, SecondCategory: SecondCategory, ThirdCategoryName: ThirdCategoryName },
            success: function (res) {
                if (res == "true") {
                    swal({
                        title: "Saved", text: "Saved Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else if (res == "updated") {
                    swal({
                        title: "Updated", text: "Updated Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else {
                    swal({ title: "Error", text: res, type: "error" })
                }
            }
        })
    }
    else {
        $("#CategoryErrorField").html("Please Enter Third Category Name")
    }

}
var Getall = function () {
    var GetallURL = $("#GetallURL").val();
    $.ajax({
        url: GetallURL,
        type: "post",
        data: {},
        success: function (res) {
            $("#DataHtml").html(res);
            $("#basic-1").DataTable();
        }
    })
}
var Edit = function (id, CategoryId, SecondLevelCategoryId, CategoryName) {
    $("#Id").val(id);
    $("#Category").val(CategoryId).change();
    $("#HideSecondCategoryId").val(SecondLevelCategoryId);
    $("#ThirdCategoryName").val(CategoryName);
    $(document).scrollTop(0)
}
var Delete = function (id) {
    var URL = $("#DeleteURL").val();
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it!'
    }).then(function () {
        $.ajax({
            url: URL,
            type: "POST",
            data: { Id: id },
            success: function (res) {
                if (res == "true") {
                    swal({
                        title: "Deleted", text: "Deleted Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else {
                    swal('Error!', res, 'error');
                }
            }
        })

    })
}