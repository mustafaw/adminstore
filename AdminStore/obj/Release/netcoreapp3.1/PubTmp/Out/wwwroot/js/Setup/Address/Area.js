﻿$(document).ready(function () {
    Getall();
})

var SaveCity = function () {
    var Id = $("#Id").val();
    var AreaName = $("#AreaName").val();
    var City = $("#City").val();
    var SaveURL = $("#SaveURL").val();
    if (AreaName != "" && City != "") {
        $.ajax({
            url: SaveURL,
            type: "post",
            data: { Id: Id, City: City, AreaName: AreaName },
            success: function (res) {
                if (res == "true") {
                    swal({
                        title: "Saved", text: "Area Saved Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else if (res == "updated") {
                    swal({
                        title: "Updated", text: "Area Updated Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else {
                    swal({ title: "Error", text: res, type: "error" })
                }
            }
        })
    }
    else {
        $("#CategoryErrorField").html("Please Enter Area Name")
    }

}
var Getall = function () {
    var GetallURL = $("#GetallURL").val();
    $.ajax({
        url: GetallURL,
        type: "post",
        data: {},
        success: function (res) {
            $("#DataHtml").html(res);
            $("#basic-1").DataTable();
        }
    })
}
var Edit = function (id, countryId, RegionId, CityId,AreaName) {
    $("#Id").val(id);
    $("#Country").val(countryId).change();
    $("#RegionId").val(RegionId)
    $("#CityId").val(CityId);
    $("#AreaName").val(AreaName);
}
var Delete = function (id) {
    var URL = $("#DeleteURL").val();
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it!'
    }).then(function () {
        $.ajax({
            url: URL,
            type: "POST",
            data: { Id: id },
            success: function (res) {
                if (res == "true") {
                    swal({
                        title: "Deleted", text: "Deleted Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else {
                    swal('Error!', res, 'error');
                }
            }
        })

    })
}