﻿$(document).ready(function () {
    Getall();
})

var AddCountry = function () {
    var Id = $("#Id").val();
    var Country = $("#Country").val();
    var Region = $("#Region").val();
    var SaveURL = $("#SaveURL").val();
    if (Country != "" && Region!="") {
        $.ajax({
            url: SaveURL,
            type: "post",
            data: { Id: Id, Country: Country, Region: Region },
            success: function (res) {
                if (res == "true") {
                    swal({
                        title: "Saved", text: "Region Saved Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else if (res == "updated") {
                    swal({
                        title: "Updated", text: "Region Updated Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else {
                    swal({ title: "Error", text: res, type: "error" })
                }
            }
        })
    }
    else {
        $("#CategoryErrorField").html("Please Enter Region Name")
    }

}
var Getall = function () {
    var GetallURL = $("#GetallURL").val();
    $.ajax({
        url: GetallURL,
        type: "post",
        data: {},
        success: function (res) {
            $("#DataHtml").html(res);
            $("#basic-1").DataTable();
        }
    })
}
var Edit = function (id, countryId,Region) {
    $("#Id").val(id);
    $("#Country").val(countryId).change();
    $("#Region").val(Region)
}
var Delete = function (id) {
    var URL = $("#DeleteURL").val();
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it!'
    }).then(function () {
        $.ajax({
            url: URL,
            type: "POST",
            data: { Id: id },
            success: function (res) {
                if (res == "true") {
                    swal({
                        title: "Deleted", text: "Deleted Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else {
                    swal('Error!', res, 'error');
                }
            }
        })

    })
}