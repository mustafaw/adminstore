﻿$(document).ready(function () {
    $("#CompanyRegtrationDate").datepicker();
    $("#Brands").select2({
        multiple: true
    });
    getallStores();
  
})

async function SaveUserDetail() {
    var Id = $("#Id").val();
    var UserID = $("#UserID").val();
    var CompanyName = $("#CompanyName").val();
    var CompanyLicence = $("#CompanyLicence").val();
    var CompanyRegNo = $("#CompanyRegNo").val();
    var CompanyRegtrationDate = $("#CompanyRegtrationDate").val();
    var CompanyNTNNo = $("#CompanyNTNNo").val();
    var ProductUlisting = $("#ProductUlisting").val();
    var Category = $("#Category").val();

   
    var Brands= JSON.stringify( $("#Brands").val());
    var BankAccountTitle = $("#BankAccountTitle").val();
    var AccountNumber = $("#AccountNumber").val();
    var BranchCode = $("#BranchCode").val();
    var Country = $("#Country").val();
    var Region = $("#Region").val();
    var City = $("#City").val();
    var Area = $("#Area").val();
    var CompanyAddress = $("#CompanyAddress").val();
    var ReturnAddress = $("#ReturnAddress").val();
    var termsAndCondition = $("input[name='termsAndCondition']:checked").val();
    var NatureOfBusiness = $("#NatureOfBusiness").val();
    var Image = await uploadProfileImage()
    if (Image == undefined || Image == "") {
        Image = $("#HideImgUrl").val()
    }

    var CompanyLicenceFile = await uploadProfileImage()
    if (CompanyLicenceFile == undefined || CompanyLicenceFile == "") {
        CompanyLicenceFile = $("#hideImgeCompanyLicence").val()
    }


    var ChequeImage = await UploadChequeImage()
    if (ChequeImage == undefined || ChequeImage == "") {
        ChequeImage = $("#HideChequeImgUrl").val()
    }
    var SaveURL = $("#SaveURL").val();
    if (CommonJs.Validate_Form('StoreForm')) {
        $.ajax({
            url: SaveURL,
            async: true,
            type: "POST",
            data: {
                Id: Id, UserID: UserID, CompanyName: CompanyName, CompanyLicence: CompanyLicence, CompanyRegNo: CompanyRegNo,
                CompanyNTNNo: CompanyNTNNo, CompanyRegtrationDate: CompanyRegtrationDate,
                ProductUlisting: ProductUlisting, NatureOfBusiness: NatureOfBusiness,
                CompanyAddress: CompanyAddress, ReturnAddress: ReturnAddress,
                termsAndCondition: termsAndCondition, Category: Category, Brands: Brands,
                BankAccountTitle: BankAccountTitle, AccountNumber: AccountNumber
                , BranchCode: BranchCode, Country: Country, CompanyLicenceFile: CompanyLicenceFile,
                Region: Region, City: City, Area: Area, Image: Image, ChequeImage: ChequeImage
            },
            success: function (res) {
                if (res == "true") {
                    swal("Success", "Store Addedd Succeffuly", "success").then(function () {
                        location.reload();
                    })
                }
                else if (res == "updated") {
                    swal("Success", "Store Updated Succeffuly", "success").then(function () {
                        location.reload();
                    })
                }
                else {
                    swal("Error", "Somethig went wrong", "error")
                }
            }
        })
    }


}
var Edit = function (id) {
    var URL = $("#EditStoreURL").val();

    $.ajax({
        url: URL,
        async: true,
        type: "POST",
        data: {
            Id: id
        },
        success: function (res) {
            console.log(res);
            $("#Id").val(res.n.id);
            $("#UserID").val(res.n.userId).change();
            $("#CompanyName").val(res.n.companyName);
            $("#CompanyLicence").val(res.n.companyLicence);
            $("#CompanyRegNo").val(res.n.companyRegNo);
            $("#CompanyRegtrationDate").val(res.n.companyRegDate);
            $("#CompanyNTNNo").val(res.n.ntnNumber);
            $("#ProductUlisting").val(res.n.noOfProductYouListing);
            $("#Category").val(res.n.countryId).change();
            var brands = []
            for (var i = 0; i < res.n.storeBrands.length; i++) {

                brands[i] = res.n.storeBrands[i].brandId;

            }

            $("#Brands").val(brands).trigger('change');
            
            $("#BankAccountTitle").val(res.n.accountTitle);
            $("#AccountNumber").val(res.n.accountNo);
            $("#BranchCode").val(res.n.branchCode);
            $("#Country").val(res.n.countryId).change();
            $("#RegionId").val(res.n.regionId);
            $("#CityId").val(res.n.cityId);
            $("#AreaId").val(res.n.areaId);
            $("#CompanyAddress").val(res.n.businessAddress);
            $("#ReturnAddress").val(res.n.returnAddress);
            if (res.n.termsAndConditions == "True") {
                $("input[name='termsAndCondition']").prop("checked","true");
            }
            $("#NatureOfBusiness").val(res.n.natureOfBusinessId).change();
            $("#HideImgUrl").val(res.n.companyLogo)
            $("#hideImgeCompanyLicence").val(res.n.licenceFile)
            $("#HideChequeImgUrl").val(res.n.chequeImage)
            $(window).scrollTop(0);

        }
    })
}
var Delete = function (id) {
    var URL = $("#DeleteURL").val();
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it!'
    }).then(function () {
        $.ajax({
            url: URL,
            type: "POST",
            data: { Id: id },
            success: function (res) {
                if (res == "true") {
                    swal({
                        title: "Deleted", text: "Deleted Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else {
                    swal('Error!', res, 'error');
                }
            }
        })

    })
}
var getallStores = function (id) {
    var URL = $("#GetallStoresURL").val();
    $.ajax({
        url: URL,
        async: true,
        type: "POST",
        data: {},
        success: function (res) {
            $("#DataHtml").html(res);
            $("#basic-1").DataTable();
        }
    })
}


///Save Profile Image
function uploadProfileImage() {
    var ImageName;
    var input = document.getElementById("CompanyLogo");
    var url = $("#SaveCompanyLogoURL").val();
    var files = input.files;
    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i != files.length; i++) {
            formData.append("files", files[i]);
        }
        $.ajax(
            {
                url: url,
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                type: "POST",
                success: function (data) {

                    ImageName = data
                }
            }
        );
    }


    return ImageName;
}

///Save Licence Image
function uploadProfileImage() {
    var ImageName;
    var input = document.getElementById("CompanyLicenceFile");
    var url = $("#SaveCompanyLicenceFileURL").val();
    var files = input.files;
    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i != files.length; i++) {
            formData.append("files", files[i]);
        }
        $.ajax(
            {
                url: url,
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                type: "POST",
                success: function (data) {

                    ImageName = data
                }
            }
        );
    }


    return ImageName;
}

///Save Cheque Image
function UploadChequeImage() {
    var ImageName;
    var input = document.getElementById("ChequeImage");
    var url = $("#SaveChequeImageURL").val();
    var files = input.files;
    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i != files.length; i++) {
            formData.append("files", files[i]);
        }
        $.ajax(
            {
                url: url,
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                type: "POST",
                success: function (data) {

                    ImageName = data
                }
            }
        );
    }


    return ImageName;
}