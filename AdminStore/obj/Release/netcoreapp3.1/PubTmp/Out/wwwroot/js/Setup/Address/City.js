﻿$(document).ready(function () {
    Getall();
})

var SaveCity = function () {
    var Id = $("#Id").val();
    var Region = $("#Region").val();
    var City = $("#City").val();
    var SaveURL = $("#SaveURL").val();
    if (Region != "" && City != "") {
        $.ajax({
            url: SaveURL,
            type: "post",
            data: { Id: Id, City: City, Region: Region },
            success: function (res) {
                if (res == "true") {
                    swal({
                        title: "Saved", text: "City Saved Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else if (res == "updated") {
                    swal({
                        title: "Updated", text: "City Updated Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else {
                    swal({ title: "Error", text: res, type: "error" })
                }
            }
        })
    }
    else {
        $("#CategoryErrorField").html("Please Enter City Name")
    }

}
var Getall = function () {
    var GetallURL = $("#GetallURL").val();
    $.ajax({
        url: GetallURL,
        type: "post",
        data: {},
        success: function (res) {
            $("#DataHtml").html(res);
            $("#basic-1").DataTable();
        }
    })
}
var Edit = function (id, countryId, RegionId,CityName) {
    $("#Id").val(id);
    $("#Country").val(countryId).change();
    $("#RegionId").val(RegionId)
    $("#City").val(CityName);
}
var Delete = function (id) {
    var URL = $("#DeleteURL").val();
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it!'
    }).then(function () {
        $.ajax({
            url: URL,
            type: "POST",
            data: { Id: id },
            success: function (res) {
                if (res == "true") {
                    swal({
                        title: "Deleted", text: "Deleted Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else {
                    swal('Error!', res, 'error');
                }
            }
        })

    })
}