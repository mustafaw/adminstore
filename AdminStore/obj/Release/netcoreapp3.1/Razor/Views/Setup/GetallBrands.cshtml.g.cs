#pragma checksum "E:\Projects\StoreAdmin\AdminStore\Views\Setup\GetallBrands.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a539db2f7395395d07570bb1b7ff1203c9a88d5d"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Setup_GetallBrands), @"mvc.1.0.view", @"/Views/Setup/GetallBrands.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\Projects\StoreAdmin\AdminStore\Views\_ViewImports.cshtml"
using AdminStore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\Projects\StoreAdmin\AdminStore\Views\_ViewImports.cshtml"
using AdminStore.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a539db2f7395395d07570bb1b7ff1203c9a88d5d", @"/Views/Setup/GetallBrands.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"acd49c3ba04e2ec959afb221d0ea6a624543ce7f", @"/Views/_ViewImports.cshtml")]
    public class Views_Setup_GetallBrands : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<AdminStore.Models.TblBrand>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "E:\Projects\StoreAdmin\AdminStore\Views\Setup\GetallBrands.cshtml"
  
    Layout = null;

#line default
#line hidden
#nullable disable
            WriteLiteral("<table class=\"display vendor-table\" id=\"basic-1\">\r\n    <thead>\r\n        <tr>\r\n            <th>Brand Name</th>\r\n            <th>Is Top</th>\r\n            <th>Action</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n");
#nullable restore
#line 14 "E:\Projects\StoreAdmin\AdminStore\Views\Setup\GetallBrands.cshtml"
         foreach (var item in Model)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n                <td>\r\n                    ");
#nullable restore
#line 18 "E:\Projects\StoreAdmin\AdminStore\Views\Setup\GetallBrands.cshtml"
               Write(item.BrandName);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n");
#nullable restore
#line 21 "E:\Projects\StoreAdmin\AdminStore\Views\Setup\GetallBrands.cshtml"
                     if (item.IsTop == true)
                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <span style=\"color:green\">Yes</span>\r\n");
#nullable restore
#line 24 "E:\Projects\StoreAdmin\AdminStore\Views\Setup\GetallBrands.cshtml"
                    }
                    else
                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <span style=\"color:red\">No</span>\r\n");
#nullable restore
#line 28 "E:\Projects\StoreAdmin\AdminStore\Views\Setup\GetallBrands.cshtml"

                    }

#line default
#line hidden
#nullable disable
            WriteLiteral("                </td>\r\n                <td>\r\n                    <div>\r\n                        <i class=\"fa fa-edit mr-2 font-success\" style=\"cursor:pointer\"");
            BeginWriteAttribute("onclick", " onclick=\"", 902, "\"", 961, 8);
            WriteAttributeValue("", 912, "Edit(\'", 912, 6, true);
#nullable restore
#line 33 "E:\Projects\StoreAdmin\AdminStore\Views\Setup\GetallBrands.cshtml"
WriteAttributeValue("", 918, item.Id, 918, 8, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 926, "\',", 926, 2, true);
            WriteAttributeValue(" ", 928, "\'", 929, 2, true);
#nullable restore
#line 33 "E:\Projects\StoreAdmin\AdminStore\Views\Setup\GetallBrands.cshtml"
WriteAttributeValue("", 930, item.BrandName, 930, 15, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 945, "\',\'", 945, 3, true);
#nullable restore
#line 33 "E:\Projects\StoreAdmin\AdminStore\Views\Setup\GetallBrands.cshtml"
WriteAttributeValue("", 948, item.IsTop, 948, 11, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 959, "\')", 959, 2, true);
            EndWriteAttribute();
            WriteLiteral("></i>\r\n                        <i class=\"fa fa-trash font-danger\" style=\"cursor:pointer\"");
            BeginWriteAttribute("onclick", " onclick=\"", 1050, "\"", 1078, 3);
            WriteAttributeValue("", 1060, "Delete(\'", 1060, 8, true);
#nullable restore
#line 34 "E:\Projects\StoreAdmin\AdminStore\Views\Setup\GetallBrands.cshtml"
WriteAttributeValue("", 1068, item.Id, 1068, 8, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1076, "\')", 1076, 2, true);
            EndWriteAttribute();
            WriteLiteral("></i>\r\n                    </div>\r\n                </td>\r\n            </tr>\r\n");
#nullable restore
#line 38 "E:\Projects\StoreAdmin\AdminStore\Views\Setup\GetallBrands.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n    </tbody>\r\n</table>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<AdminStore.Models.TblBrand>> Html { get; private set; }
    }
}
#pragma warning restore 1591
