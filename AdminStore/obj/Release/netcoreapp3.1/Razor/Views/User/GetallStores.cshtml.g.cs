#pragma checksum "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallStores.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a21c32b1d54d20b967a559e0c2ec19ba1880a2fd"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_User_GetallStores), @"mvc.1.0.view", @"/Views/User/GetallStores.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\Projects\StoreAdmin\AdminStore\Views\_ViewImports.cshtml"
using AdminStore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallStores.cshtml"
using AdminStore.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a21c32b1d54d20b967a559e0c2ec19ba1880a2fd", @"/Views/User/GetallStores.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"acd49c3ba04e2ec959afb221d0ea6a624543ce7f", @"/Views/_ViewImports.cshtml")]
    public class Views_User_GetallStores : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<AdminStore.Models.TblStore>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallStores.cshtml"
  
    Layout = null;

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<table class=""display vendor-table"" id=""basic-1"">
    <thead>
        <tr>
            <th>Date</th>
            <th>User Name</th>
            <th>Company Name</th>
            <th>Category</th>
            <th>Nature Of Business</th>
         
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
");
#nullable restore
#line 19 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallStores.cshtml"
         foreach (var item in Model)
        {
            StoreKingbhaiContext db = new StoreKingbhaiContext();
            var getuserDetail = db.TblUserDetail.FirstOrDefault(x => x.UserId == item.UserId);
            var getcategory = db.TblCategory.FirstOrDefault(x => x.Id == item.CategoryId);
            var getnatureofBusiness = db.TblNatureOfBusiness.FirstOrDefault(x => x.Id == item.NatureOfBusinessId);


#line default
#line hidden
#nullable disable
            WriteLiteral("        <tr>\r\n            <td>\r\n                ");
#nullable restore
#line 28 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallStores.cshtml"
           Write(Convert.ToDateTime(item.CreatedOn).ToString("dd/MM/yyyy"));

#line default
#line hidden
#nullable disable
            WriteLiteral(";\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 31 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallStores.cshtml"
           Write(getuserDetail.FullName);

#line default
#line hidden
#nullable disable
            WriteLiteral(" ( ");
#nullable restore
#line 31 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallStores.cshtml"
                                     Write(getuserDetail.Cnic);

#line default
#line hidden
#nullable disable
            WriteLiteral(")\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 34 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallStores.cshtml"
           Write(item.CompanyName);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 37 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallStores.cshtml"
           Write(getcategory.CategoryName);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 40 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallStores.cshtml"
           Write(getnatureofBusiness.NatureOfBusiness);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                <div>\r\n                    <i class=\"fa fa-edit mr-2 font-success\" style=\"cursor:pointer\"");
            BeginWriteAttribute("onclick", " onclick=\"", 1445, "\"", 1471, 3);
            WriteAttributeValue("", 1455, "Edit(\'", 1455, 6, true);
#nullable restore
#line 44 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallStores.cshtml"
WriteAttributeValue("", 1461, item.Id, 1461, 8, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1469, "\')", 1469, 2, true);
            EndWriteAttribute();
            WriteLiteral("></i>\r\n                    <i class=\"fa fa-trash font-danger\" style=\"cursor:pointer\"");
            BeginWriteAttribute("onclick", " onclick=\"", 1556, "\"", 1584, 3);
            WriteAttributeValue("", 1566, "Delete(\'", 1566, 8, true);
#nullable restore
#line 45 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallStores.cshtml"
WriteAttributeValue("", 1574, item.Id, 1574, 8, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1582, "\')", 1582, 2, true);
            EndWriteAttribute();
            WriteLiteral("></i>\r\n                </div>\r\n            </td>\r\n        </tr>\r\n");
#nullable restore
#line 49 "E:\Projects\StoreAdmin\AdminStore\Views\User\GetallStores.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n    </tbody>\r\n</table>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<AdminStore.Models.TblStore>> Html { get; private set; }
    }
}
#pragma warning restore 1591
