﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AdminStore.Models;
using System.IO;
using Microsoft.AspNetCore.Http;
using AdminStore.Services;

namespace AdminStore.Controllers
{
    [Authorize(Roles = "Admin, DataEntry")]
    public class SetupController : Controller
    {
        string path = "";
        private StoreKingbhaiContext db = new StoreKingbhaiContext();
        public SetupController()
        {
            getpath();
        }
        public void getpath()
        {
            PathClass obj = new PathClass();
            path = obj.getpthString();
        }
        public IActionResult Category()
        {
            return View();
        }
        public IActionResult SecondCategory()
        {
            return View();
        }
        public IActionResult ThirdCategory()
        {
            return View();
        }
        public IActionResult FourthCategory()
        {
            return View();
        }
        public IActionResult UOM()
        {
            return View();
        }
        public IActionResult Country()
        {
            
            return View();
        }
        public IActionResult Region()
        {
            return View();
        }
        public IActionResult City()
        {
            return View();
        }
        public IActionResult Area()
        {
            return View();
        }
        public IActionResult Brand()
        {
            return View();
        }
        //UOM
        public async Task<string> SaveUOM(string Id, string UOM)
        {
            try
            {
                if (Id == "0")
                {
                    var n = new TblProductUnit();
                    n.UnitName = UOM;
                    db.TblProductUnit.Add(n);
                    await db.SaveChangesAsync();
                    return "true";
                }
                else
                {
                    var CID = Convert.ToDecimal(Id);
                    var get = db.TblProductUnit.FirstOrDefault(x => x.Id == CID);
                    get.UnitName = UOM;
                    db.TblProductUnit.Update(get);
                    await db.SaveChangesAsync();
                    return "updated";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public IActionResult GetallUOM()
        {
            var get = db.TblProductUnit.ToList();
            return PartialView(get);
        }
        public string DeleteUOM(decimal Id)
        {
            try
            {
                var get = db.TblProductUnit.FirstOrDefault(x => x.Id == Id);
                db.TblProductUnit.Remove(get);
                db.SaveChanges();
                return "true";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        //catgory
        public async Task< string> SaveCategory(string Id, string Category,string ImageOne,string IsFront,string OrderBy)
        {
            try
            {
             
                if(Id=="0")
                {
                   
                    var n = new TblCategory();
                    n.CategoryName = Category;
                    if(IsFront== "on")
                    {
                        n.IsTop = true;
                    }
                    else
                    {
                        n.IsTop = false;
                    }
                    if (OrderBy != ""&&OrderBy!=null)
                    {
                        n.Orderby = Convert.ToInt32(OrderBy);
                    }
                    else
                    {
                        var getmax = db.TblCategory.Max(x => x.Orderby);
                        n.Orderby = getmax + 1;
                    }
                    n.Image = ImageOne;
                    db.TblCategory.Add(n);
                    await db.SaveChangesAsync();
                    return "true";
                }
                else
                {
                    var CID = Convert.ToDecimal(Id);
                    var get = db.TblCategory.FirstOrDefault(x => x.Id == CID);
                    get.CategoryName = Category;
                    if (IsFront == "on")
                    {
                        get.IsTop = true;
                    }
                    else
                    {
                        get.IsTop = false;
                    }
                    if (OrderBy != "" && OrderBy != null)
                    {
                       get.Orderby = Convert.ToInt32(OrderBy);
                    }
                    else
                    {
                        var getmax = db.TblCategory.Max(x => x.Orderby);
                        get.Orderby = getmax + 1;
                    }
                    get.Image = ImageOne;
                    db.TblCategory.Update(get);
                    await db.SaveChangesAsync();
                    return "updated";
                }
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }
        public IActionResult GetAllCategory()
        {
            var get = db.TblCategory.ToList();
            return PartialView(get);
        }
        public string DeleteCategory(decimal Id)
        {
            try
            {
                var get = db.TblCategory.FirstOrDefault(x => x.Id == Id);
                db.TblCategory.Remove(get);
                db.SaveChanges();
                return "true";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }
        //Brand

        public async Task<string> SaveBrand(string Id, string Brand,string IsTop)
        {
            try
            {
                if (Id == "0")
                {
                    var n = new TblBrand();
                    n.BrandName = Brand;
                    if (IsTop == "on")
                    {
                        n.IsTop = true;
                    }
                    else
                    {
                        n.IsTop = false;
                    }
                    db.TblBrand.Add(n);
                    await db.SaveChangesAsync();
                    return "true";
                }
                else
                {
                    var CID = Convert.ToDecimal(Id);
                    var get = db.TblBrand.FirstOrDefault(x => x.Id == CID);
                    get.BrandName = Brand;
                    if (IsTop == "on")
                    {
                        get.IsTop = true;
                    }
                    else
                    {
                        get.IsTop = false;
                    }
                    db.TblBrand.Update(get);
                    await db.SaveChangesAsync();
                    return "updated";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public IActionResult GetallBrands()
        {
            var get = db.TblBrand.ToList();
            return PartialView(get);
        }
        public string DeleteBrand(decimal Id)
        {
            try
            {
                var get = db.TblBrand.FirstOrDefault(x => x.Id == Id);
                db.TblBrand.Remove(get);
                db.SaveChanges();
                return "true";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        //////
        //country
        public async Task<string> SaveCountry(string Id, string Country)
        {
            try
            {
                if (Id == "0")
                {
                    var n = new TblCountry();
                    n.CountryName = Country;
                    db.TblCountry.Add(n);
                    await db.SaveChangesAsync();
                    return "true";
                }
                else
                {
                    var CID = Convert.ToDecimal(Id);
                    var get = db.TblCountry.FirstOrDefault(x => x.Id == CID);
                    get.CountryName = Country;
                    db.TblCountry.Update(get);
                    await db.SaveChangesAsync();
                    return "updated";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public IActionResult GetallCountry()
        {
            var get = db.TblCountry.ToList();
            return PartialView(get);
        }
        public string DeleteCountry(decimal Id)
        {
            try
            {
                var get = db.TblCountry.FirstOrDefault(x => x.Id == Id);
                db.TblCountry.Remove(get);
                db.SaveChanges();
                return "true";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        //////
      
            //Region

        public async Task<string> SaveRegion(string Id, string Country,string Region)
        {
            try
            {
                if (Id == "0")
                {
                    var n = new TblRegion();
                    n.CountryId = Convert.ToDecimal(Country);
                    n.RegionName = Region;
                    db.TblRegion.Add(n);
                    await db.SaveChangesAsync();
                    return "true";
                }
                else
                {
                    var CID = Convert.ToDecimal(Id);
                    var get = db.TblRegion.FirstOrDefault(x => x.Id == CID);
                    get.CountryId = Convert.ToDecimal(Country);
                    get.RegionName = Region;
                    db.TblRegion.Update(get);
                    await db.SaveChangesAsync();
                    return "updated";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public IActionResult GetallRegion()
        {
            var get = db.TblRegion.ToList();
            return PartialView(get);
        }
        public string DeleteRegion(decimal Id)
        {
            try
            {
                var get = db.TblRegion.FirstOrDefault(x => x.Id == Id);
                db.TblRegion.Remove(get);
                db.SaveChanges();
                return "true";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        //////
        ///
        public async Task<string> SaveCity(string Id, string City, string Region)
        {
            try
            {
                if (Id == "0")
                {
                    var n = new TblCity();
                    n.RegionId = Convert.ToDecimal(Region);
                    n.CityName = City;
                    db.TblCity.Add(n);
                    await db.SaveChangesAsync();
                    return "true";
                }
                else
                {
                    var CID = Convert.ToDecimal(Id);
                    var get = db.TblCity.FirstOrDefault(x => x.Id == CID);
                    get.RegionId = Convert.ToDecimal(Region);
                    get.CityName = City;
                    db.TblCity.Update(get);
                    await db.SaveChangesAsync();
                    return "updated";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public IActionResult GetallCity()
        {
            var get = db.TblCity.ToList();
            return PartialView(get);
        }
        public string DeleteCity(decimal Id)
        {
            try
            {
                var get = db.TblCity.FirstOrDefault(x => x.Id == Id);
                db.TblCity.Remove(get);
                db.SaveChanges();
                return "true";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        ///
        public async Task<string> SaveArea(string Id, string City, string AreaName)
        {
            try
            {
                if (Id == "0")
                {
                    var n = new TblArea();
                    n.CityId = Convert.ToDecimal(City);
                    n.AreaName = AreaName;
                    db.TblArea.Add(n);
                    await db.SaveChangesAsync();
                    return "true";
                }
                else
                {
                    var CID = Convert.ToDecimal(Id);
                    var get = db.TblArea.FirstOrDefault(x => x.Id == CID);
                    get.CityId = Convert.ToDecimal(City);
                    get.AreaName = AreaName;
                    db.TblArea.Update(get);
                    await db.SaveChangesAsync();
                    return "updated";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public IActionResult GetallArea()
        {
            var get = db.TblArea.ToList();
            return PartialView(get);
        }
        public string DeleteArea(decimal Id)
        {
            try
            {
                var get = db.TblArea.FirstOrDefault(x => x.Id == Id);
                db.TblArea.Remove(get);
                db.SaveChanges();
                return "true";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //SecondCategory

        public async Task<string> SaveSecondCategory(string Id, string Category, string SecondCateName)
        {
            try
            {
                if (Id == "0")
                {
                    var n = new TblSecondLevelCategory();
                    n.CategoryName = SecondCateName;
                    n.CategoryId = Convert.ToDecimal(Category); 
                    db.TblSecondLevelCategory.Add(n);
                    await db.SaveChangesAsync();
                    return "true";
                }
                else
                {
                    var CID = Convert.ToDecimal(Id);
                    var n = db.TblSecondLevelCategory.FirstOrDefault(x => x.Id == CID);
                    n.CategoryName = SecondCateName;
                    n.CategoryId = Convert.ToDecimal(Category);
                    db.TblSecondLevelCategory.Update(n);
                    await db.SaveChangesAsync();
                    return "updated";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public IActionResult GetallSecondCategory()
        {
            var get = db.TblSecondLevelCategory.ToList();
            return PartialView(get);
        }
        public string DeleteSecondCategory(decimal Id)
        {
            try
            {
                var get = db.TblSecondLevelCategory.FirstOrDefault(x => x.Id == Id);
                db.TblSecondLevelCategory.Remove(get);
                db.SaveChanges();
                return "true";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //Third Category
        public async Task<string> SaveThirdCategory(string Id, string SecondCategory, string ThirdCategoryName)
        {
            try
            {
                if (Id == "0")
                {
                    var n = new TblThirdLevelCategory();
                    n.CategoryName = ThirdCategoryName;
                    n.SecondLevelCategoryId = Convert.ToDecimal(SecondCategory);
                    db.TblThirdLevelCategory.Add(n);
                    await db.SaveChangesAsync();
                    return "true";
                }
                else
                {
                    var CID = Convert.ToDecimal(Id);
                    var n = db.TblThirdLevelCategory.FirstOrDefault(x => x.Id == CID);
                    n.CategoryName = ThirdCategoryName;
                    n.SecondLevelCategoryId = Convert.ToDecimal(SecondCategory);
                    db.TblThirdLevelCategory.Update(n);
                    await db.SaveChangesAsync();
                    return "updated";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public IActionResult GetallThirdCategory()
        {
            var get = db.TblThirdLevelCategory.ToList();
            return PartialView(get);
        }
        public string DeleteThirdCategory(decimal Id)
        {
            try
            {
                var get = db.TblThirdLevelCategory.FirstOrDefault(x => x.Id == Id);
                db.TblThirdLevelCategory.Remove(get);
                db.SaveChanges();
                return "true";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //Fourth Category
        public async Task<string> SaveFourthCategory(string Id, string ThirdCategory, string ForthCategoryName)
        {
            try
            {
                if (Id == "0")
                {
                    var n = new TblForthLevelCategory();
                    n.CategoryName = ForthCategoryName;
                    n.TblThirdLevelCategoryId = Convert.ToDecimal(ThirdCategory);
                    db.TblForthLevelCategory.Add(n);
                    await db.SaveChangesAsync();
                    return "true";
                }
                else
                {
                    var CID = Convert.ToDecimal(Id);
                    var n = db.TblForthLevelCategory.FirstOrDefault(x => x.Id == CID);
                    n.CategoryName = ForthCategoryName;
                    n.TblThirdLevelCategoryId = Convert.ToDecimal(ThirdCategory);
                    db.TblForthLevelCategory.Update(n);
                    await db.SaveChangesAsync();
                    return "updated";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public IActionResult GetallFourthCategory()
        {
            var get = db.TblForthLevelCategory.ToList();
            return PartialView(get);
        }
        public string DeleteFourthCategory(decimal Id)
        {
            try
            {
                var get = db.TblForthLevelCategory.FirstOrDefault(x => x.Id == Id);
                db.TblForthLevelCategory.Remove(get);
                db.SaveChanges();
                return "true";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public string GetallRegionDRP(string Country)
        {
            var html = "<option value=''>--Select Region--</option>";
            try
            {
                var Id = Convert.ToDecimal(Country);
                var getallRegion = db.TblRegion.Where(X => X.CountryId == Id).ToList();
                foreach(var item in getallRegion)
                {
                    html = html + "<option value=" + item.Id + ">" + item.RegionName + "</option>";
                }
                return html;
            }
            catch(Exception ex)
            {
                html= "<option value=''>--Select Region--</option>";
            }
            return html;
        }
        public string GetallCityDRP(string Region)
        {
            var html = "<option value=''>--Select City--</option>";
            try
            {
                var Id = Convert.ToDecimal(Region);
                var getallRegion = db.TblCity.Where(X => X.RegionId == Id).ToList();
                foreach (var item in getallRegion)
                {
                    html = html + "<option value=" + item.Id + ">" + item.CityName + "</option>";
                }
                return html;
            }
            catch (Exception ex)
            {
                html = "<option value=''>--Select City--</option>";
            }
            return html;
        }
        public string GetallAreaDRP(string City)
        {
            var html = "<option value=''>--Select Area--</option>";
            try
            {
                var Id = Convert.ToDecimal(City);
                var getallRegion = db.TblArea.Where(X => X.CityId == Id).ToList();
                foreach (var item in getallRegion)
                {
                    html = html + "<option value=" + item.Id + ">" + item.AreaName + "</option>";
                }
                return html;
            }
            catch (Exception ex)
            {
                html = "<option value=''>--Select Area--</option>";
            }
            return html;
        }

        //CategoryDRP

        public string GetSecondCategoryDRP(string CategoryId)
        {
            var html = "<option value=''>--Select Second Category--</option>";
            try
            {
                var Id = Convert.ToDecimal(CategoryId);
                var getall = db.TblSecondLevelCategory.Where(X => X.CategoryId == Id).ToList();
                foreach (var item in getall)
                {
                    html = html + "<option value=" + item.Id + ">" + item.CategoryName + "</option>";
                }
                return html;
            }
            catch (Exception ex)
            {
                html = "<option value=''>--Select Second Category--</option>";
            }
            return html;
        }
        public string GetThirdCategoryDRP(string SecondCategoryId)
        {
            var html = "<option value=''>--Select Third Category--</option>";
            try
            {
                var Id = Convert.ToDecimal(SecondCategoryId);
                var getall = db.TblThirdLevelCategory.Where(X => X.SecondLevelCategoryId == Id).ToList();
                foreach (var item in getall)
                {
                    html = html + "<option value=" + item.Id + ">" + item.CategoryName + "</option>";
                }
                return html;
            }
            catch (Exception ex)
            {
                html = "<option value=''>--Select Third Category--</option>";
            }
            return html;
        }
        public string GetFourthCategoryDRP(string ThirdCategoryID)
        {
            var html = "<option value=''>--Select Fourth Category--</option>";
            try
            {
                var Id = Convert.ToDecimal(ThirdCategoryID);
                var getall = db.TblForthLevelCategory.Where(X => X.TblThirdLevelCategoryId == Id).ToList();
                foreach (var item in getall)
                {
                    html = html + "<option value=" + item.Id + ">" + item.CategoryName + "</option>";
                }
                return html;
            }
            catch (Exception ex)
            {
                html = "<option value=''>--Select Fourth Category--</option>";
            }
            return html;
        }

        public IActionResult EditCategory(decimal Id)
        {
            var n = db.TblCategory.FirstOrDefault(x => x.Id == Id);
            return Json(new { n});
        }
        public async Task<JsonResult> SaveCategoryImage(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);
            var UniqueName = "";
            foreach (var formFile in files)
            {

                try
                {
                    if (formFile.Length > 0)
                    {
                        string UploadsFolder = Path.Combine(path, "Images\\CategoryImages");
                        if (!Directory.Exists(UploadsFolder))
                        {
                            Directory.CreateDirectory(UploadsFolder);
                        }
                        UniqueName = Guid.NewGuid() + "_" + formFile.FileName;
                        string filePath = Path.Combine(UploadsFolder, UniqueName);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(stream);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Json("");
                }

            }
            return Json("Images/CategoryImages/" + UniqueName);

        }
    }
}