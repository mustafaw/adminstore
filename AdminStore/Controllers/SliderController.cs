﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AdminStore.Models;
using Microsoft.AspNetCore.Authorization;
using AdminStore.Services;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Text.Json;
namespace AdminStore.Controllers
{
    [Authorize(Roles = "Admin, DataEntry")]
    public class SliderController : Controller
    {
        string path = "";
        private StoreKingbhaiContext db = new StoreKingbhaiContext();
        public SliderController()
        {
            getpath();
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult SubSlider()
        {
            return View();
        }
        public IActionResult WebReviews()
        {
            return View();
        }
        public IActionResult DiscountBanner()
        {
            return View();
        }
        public void getpath()
        {
            PathClass obj = new PathClass();
            path = obj.getpthString();
        }
        public async Task< string> SaveMainSlider(string Id, string Title, string Subtitle, string Description, string Link, string ImageOne, string ImageTwo)
        {
            string res = "";
            try
            {
                if(Id=="0")
                {
                    var s = new TblMainbanner();
                    s.Title = Title;
                    s.SubTitle = Subtitle;
                    s.Description = Description;
                    s.Link = Link;
                    s.FirstImage = ImageOne;
                    s.SecondImage = ImageTwo;
                    db.TblMainbanner.Add(s);
                    await db.SaveChangesAsync();

                    res = "true";
                }
                else
                {
                    var SId = Convert.ToDecimal(Id);
                    var s = db.TblMainbanner.FirstOrDefault(x=>x.Id== SId);
                    s.Title = Title;
                    s.SubTitle = Subtitle;
                    s.Description = Description;
                    s.Link = Link;
                    s.FirstImage = ImageOne;
                    s.SecondImage = ImageTwo;
                    db.TblMainbanner.Update(s);
                    await db.SaveChangesAsync();
                    res = "updated";
                }
              
            }
            catch(Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }
        public IActionResult EditMainSlider(decimal Id)
        {
            var s = db.TblMainbanner.FirstOrDefault(x => x.Id == Id);
            return Json(new { s });
        }
        public string DeleteMainSlider(decimal Id)
        {
            try
            {
                var s = db.TblMainbanner.FirstOrDefault(x => x.Id == Id);
                db.TblMainbanner.Remove(s);
                db.SaveChanges();
                return "true";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
           
        }
        public async Task<string> SaveSubSlider(string Id, string Title, string Subtitle, string Position, string Link, string ImageOne)
        {
            string res = "";
            try
            {
                if (Id == "0")
                {
                    var s = new TblSubbanners();
                    s.Title = Title;
                    s.SubTitle = Subtitle;
                    if (Position== "Top")
                    {
                        s.IsBottom = false ;
                        s.IsTop = true;
                    }
                    if (Position == "Bottom")
                    {
                        s.IsBottom = true;
                        s.IsTop = false;

                    }
                 
                    s.Link = Link;
                    s.Image = ImageOne;
                    db.TblSubbanners.Add(s);
                    await db.SaveChangesAsync();

                    res = "true";
                }
                else
                {
                    var SId = Convert.ToDecimal(Id);
                    var s = db.TblSubbanners.FirstOrDefault(x => x.Id == SId);
                    
                    s.Title = Title;
                    s.SubTitle = Subtitle;
                    if (Position == "Top")
                    {
                        s.IsBottom = false;
                        s.IsTop = true;
                    }
                    if (Position == "Bottom")
                    {
                        s.IsBottom = true;
                        s.IsTop = false;

                    }

                    s.Link = Link;
                    s.Image = ImageOne;
                    db.TblSubbanners.Update(s);
                    await db.SaveChangesAsync();
                    res = "updated";
                }

            }
            catch (Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }
        public IActionResult EditSubSlider(decimal Id)
        {
            var s = db.TblSubbanners.FirstOrDefault(x => x.Id == Id);
            return Json(new { s });
        }
        public string DeleteSubSlider(decimal Id)
        {
            try
            {
                var s = db.TblSubbanners.FirstOrDefault(x => x.Id == Id);
                db.TblSubbanners.Remove(s);
                db.SaveChanges();
                return "true";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        public async Task<string> SaveDiscountBanner(string Id, string Title, string Link, string ImageOne)
        {
            string res = "";
            try
            {
                if (Id == "0")
                {
                    var s = new TblDiscountbanner();
                    s.Title = Title;
                  
                    s.Link = Link;
                    s.Image = ImageOne;
                 
                    db.TblDiscountbanner.Add(s);
                    await db.SaveChangesAsync();

                    res = "true";
                }
                else
                {
                    var SId = Convert.ToDecimal(Id);
                    var s = db.TblDiscountbanner.FirstOrDefault(x => x.Id == SId);
                
                    s.Title = Title;

                    s.Link = Link;
                    s.Image = ImageOne;
                    db.TblDiscountbanner.Update(s);
                    await db.SaveChangesAsync();
                    res = "updated";
                }

            }
            catch (Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }
        public IActionResult EditDiscountBanner(decimal Id)
        {
            var s = db.TblDiscountbanner.FirstOrDefault(x => x.Id == Id);
            return Json(new { s });
        }
        public string DeleteDiscountBanner(decimal Id)
        {
            try
            {
                var s = db.TblDiscountbanner.FirstOrDefault(x => x.Id == Id);
                db.TblDiscountbanner.Remove(s);
                db.SaveChanges();
                return "true";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        public async Task< string> ShowFront(decimal Id)
        {
            try
            {
                var get = db.TblWebReviews.FirstOrDefault(x => x.Id == Id);
                get.IsFront = true;
                db.TblWebReviews.Update(get);
              await  db.SaveChangesAsync();
                return "true";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
           
        }
        public async Task<string> ShowNotFrontURL(decimal Id)
        {
            try
            {
                var get = db.TblWebReviews.FirstOrDefault(x => x.Id == Id);
                get.IsFront = false;
                db.TblWebReviews.Update(get);
                await db.SaveChangesAsync();
                return "true";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        public async Task<JsonResult> SaveCompanyLogo(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);
            var UniqueName = "";
            foreach (var formFile in files)
            {

                try
                {
                    if (formFile.Length > 0)
                    {
                        string UploadsFolder = Path.Combine(path, "Images\\CompanyImages");
                        if (!Directory.Exists(UploadsFolder))
                        {
                            Directory.CreateDirectory(UploadsFolder);
                        }
                        UniqueName = Guid.NewGuid() + "_" + formFile.FileName;
                        string filePath = Path.Combine(UploadsFolder, UniqueName);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(stream);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Json("");
                }

            }
            return Json("Images/CompanyImages/" + UniqueName);

        }
        public async Task<JsonResult> SaveImageOne(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);
            var UniqueName = "";
            foreach (var formFile in files)
            {

                try
                {
                    if (formFile.Length > 0)
                    {
                        string UploadsFolder = Path.Combine(path, "Images\\Slider");
                        if (!Directory.Exists(UploadsFolder))
                        {
                            Directory.CreateDirectory(UploadsFolder);
                        }
                        UniqueName = Guid.NewGuid() + "_" + formFile.FileName;
                        string filePath = Path.Combine(UploadsFolder, UniqueName);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(stream);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Json("");
                }

            }
            return Json("Images/Slider/" + UniqueName);

        }
        public async Task<JsonResult> SaveImageTwo(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);
            var UniqueName = "";
            foreach (var formFile in files)
            {

                try
                {
                    if (formFile.Length > 0)
                    {
                        string UploadsFolder = Path.Combine(path, "Images\\Slider");
                        if (!Directory.Exists(UploadsFolder))
                        {
                            Directory.CreateDirectory(UploadsFolder);
                        }
                        UniqueName = Guid.NewGuid() + "_" + formFile.FileName;
                        string filePath = Path.Combine(UploadsFolder, UniqueName);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(stream);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Json("");
                }

            }
            return Json("Images/Slider/" + UniqueName);

        }
    }
}