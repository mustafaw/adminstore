﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AdminStore.Models;
using Microsoft.AspNetCore.Identity;
using AdminStore.Services;
using Microsoft.AspNetCore.Authorization;

namespace AdminStore.Controllers
{
    [Authorize(Roles = "Admin, DataEntry")]
    public class HomeController : Controller
    {
        private readonly RoleManager<IdentityRole> roleManger;
        private readonly UserManager<IdentityUser> UserManger;
        public HomeController(UserManager<IdentityUser> userMange, RoleManager<IdentityRole> RoleManger)
        {
            this.roleManger = RoleManger;
            this.UserManger = userMange;
        }
        public async Task<string> CreateRole()
        {
            string[] str = { "DataEntry" };
            foreach (var item in str)
            {
                IdentityService d = new IdentityService(UserManger, roleManger);
                var n = await d.CreateRole(item);
            }
            return "true";

        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Test()
        {
            return View();
        }
    }
}
