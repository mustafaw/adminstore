﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AdminStore.Models;
using Microsoft.AspNetCore.Authorization;
using AdminStore.Services;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;

namespace AdminStore.Controllers
{

    [Authorize(Roles = "Admin, DataEntry")]
    public class UserController : Controller
    {
        string path = "";
        private StoreKingbhaiContext db = new StoreKingbhaiContext();
        private IHostingEnvironment hostingEnviroment;
        
        public UserController(IHostingEnvironment hostingEnv)
        {
            hostingEnviroment = hostingEnv;
           this.getpath();
        }
        public void getpath()
        {
            PathClass obj = new PathClass();
            path = obj.getpthString();
        }
        public IActionResult CustomUsers()
        {
            return View();
        }
        public IActionResult Product()
        {
            return View();
        }
        public IActionResult Stores()
        {
            return View();
        }
        public IActionResult AddUserDetail()
        {
            return View();
        }
        public IActionResult ProductImages()
        {
            return View();
        }
        public async Task<string> BlockUser(string Id)
        {
            try
            {
                var get = db.AspNetUsers.FirstOrDefault(x => x.Id == Id);
                get.EmailConfirmed = false;
                db.AspNetUsers.Update(get);
                await db.SaveChangesAsync();
                return "true";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public async Task<string> UnblockUser(string Id)
        {
            try
            {
                var get = db.AspNetUsers.FirstOrDefault(x => x.Id == Id);
                get.EmailConfirmed = true ;
                db.AspNetUsers.Update(get);
                await db.SaveChangesAsync();
                return "true";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public async Task<string> DeleteUser(string Id)
        {
            try
            {
                var get = db.AspNetUsers.FirstOrDefault(x => x.Id == Id);
                db.AspNetUsers.Remove(get);
               await db.SaveChangesAsync();
                return "true";

            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }
        public async Task<string> DeleteStore(decimal Id)
        {
            try
            {
                var get = db.TblStore.FirstOrDefault(x => x.Id == Id);
                db.TblStore.Remove(get);
                await db.SaveChangesAsync();
                return "true";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }
        public IActionResult GetallUser()
        {
            List<UserViewModel> list = new List<UserViewModel>();
            var getallUser = db.AspNetUsers.Where(x => x.AspNetUserRoles.FirstOrDefault().Role.Name != "Admin").ToList();
            foreach (var item in getallUser)
            {
                var getDetail = db.TblUserDetail.FirstOrDefault(x => x.UserId == item.Id);
                var getstore = db.TblStore.FirstOrDefault(x => x.UserId == item.Id);
                var n = new UserViewModel();
                n.Id = item.Id;
                n.Email = item.Email;
                n.PhoneNumber = item.PhoneNumber;
                n.UserName = item.UserName;
                if (getDetail != null)
                {
                    n.FullName = getDetail.FullName;
                    n.Cnic = getDetail.Cnic;
                }
                if(getstore==null)
                {
                    n.UserType = "Buyer";
                    n.StoreId = "";
                }
                else
                {
                    n.UserType = "Seller";
                    n.StoreId = getstore.Id.ToString();
                }
                list.Add(n);
            }
            return PartialView(list);
        }
        public IActionResult GetallStores()
        {
            var getallStores = db.TblStore.ToList();
            return PartialView(getallStores);
        }
        public IActionResult GetallProducts()
        {
            var getall = db.TblProduct.ToList();
            return PartialView(getall);
        }
        public async Task<string > DeleteProduct(decimal Id)
        {
            try
            {
                var getproduct = db.TblProduct.FirstOrDefault(x => x.Id == Id);
                db.TblProduct.Remove(getproduct);
                await db.SaveChangesAsync();
                return "true";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }
        public IActionResult GetallProductImages()
        {
            //var list1 = db.TblProductImages.FromSqlRaw("[dbo].[ProcGetProductImagesAdmin]").ToList();

            var getall = db.TblProductImages.GroupBy(x => x.ProductId).Select(g => new 
            {
                ProductId = g.Key,
                count = g.Count()
            }

                ).ToList();
            List<ProductImages> list = new List<ProductImages>();
            foreach (var e in getall)
            {
                var n = new ProductImages();
                var item = db.TblProduct.FirstOrDefault(x => x.Id == e.ProductId);
                var category = db.TblCategory.FirstOrDefault(x => x.Id == item.CategoryId);
                var brand = db.TblBrand.FirstOrDefault(x => x.Id == item.BrandId);
                var user = db.TblUserDetail.FirstOrDefault(x => x.UserId == item.UserId);
                var store = db.TblStore.FirstOrDefault(x => x.Id == item.StoreId);
                n.CreateOn = Convert.ToDateTime(item.CreatedOn).ToString("dd/MMM/yyyy");
                n.ProductName = item.ProductName;
                n.Category = category.CategoryName;
                n.UserName = user.FullName;
                n.Thumbnails = item.ThumbNailImage;
                n.Brand = brand.BrandName;
                n.storeName = store.CompanyName;
                n.ProductId = item.Id.ToString();
                list.Add(n);   

            }
            return PartialView(list);
        }
        public IActionResult ViewProductImages(decimal id)
        {
            var getall = db.TblProductImages.Where(x=>x.ProductId==id).ToList();
            return PartialView(getall);
        }
        public string RemoveProductImage( decimal Id)
        {
            try
            {
                var get = db.TblProductImages.FirstOrDefault(x => x.Id == Id);
                string UploadsFolder = Path.Combine(path, "Images\\Products");
                if (!Directory.Exists(UploadsFolder))
                {
                    Directory.CreateDirectory(UploadsFolder);
                }
                string filePath = Path.Combine(UploadsFolder, get.ImagePath);
                if ((System.IO.File.Exists(filePath)))
                {
                    System.IO.File.Delete(filePath);
                }
                db.TblProductImages.Remove(get);
                db.SaveChanges();
                return "true";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            
            

        }
        public IActionResult EditStore(decimal Id)
        {
            var item = db.TblStore.FirstOrDefault(x => x.Id == Id);
            var getAddress = db.TblAddress.FirstOrDefault(x => x.StoreId == item.Id);
          
            var getbank = db.TblBankDetail.FirstOrDefault(x => x.StoreId == item.Id);
            var getbrands = db.TblStoreBrand.Where(x => x.StoreId == item.Id).ToList() ;
            var n = new StoreViewModel();
            n.Id = item.Id.ToString(); ;
            n.userId = item.UserId;
            n.CompanyName = item.CompanyName;
            n.CompanyLogo = item.CompanyLogo;
            n.CompanyLicence = item.CompanyLicence;
            if(getAddress!=null)
            {
                n.AddressId = getAddress.Id.ToString(); ;
                if (getAddress.CountryId != null)
                {
                    n.CountryId = getAddress.CountryId.ToString();
                }
                if (getAddress.RegionId != null)
                {
                    n.RegionId = getAddress.RegionId.ToString();
                }
                if (getAddress.CityId != null)
                {
                    n.CityId = getAddress.CityId.ToString();
                }
                if (getAddress.AreaId != null)
                {
                    n.AreaId = getAddress.AreaId.ToString();
                }
                n.LocationName = getAddress.LocationName;
                n.AddressDescription = getAddress.AddressDescription;


            }
            if(getbank!=null)
            {
                n.bankId = getbank.Id.ToString();
                n.AccountTitle = getbank.AccountTitle;
                n.AccountNo = getbank.AccountNo;
                n.BranchCode = getbank.BranchCode;
                n.ChequeImage = getbank.ChequeImage;
            }
            
            n.LicenceFile = item.LicenceFile;
            n.CompanyRegNo = item.CompanyRegNo;
            if(item.CompanyRegDate!=null)
            {
                n.CompanyRegDate = Convert.ToDateTime(item.CompanyRegDate).ToString("MMM/dd/yyyy"); ;
            }
           
            n.NatureOfBusinessId = item.NatureOfBusinessId.ToString(); ;
            n.NTNNumber = item.Ntnnumber;
            n.BusinessAddress = item.BusinessAddress;
            n.ReturnAddress = item.ReturnAddress;
            n.TermsAndConditions = item.TermsAndConditions.ToString() ;
            n.NoOfProductYouListing = item.NoOfProductYouListing.ToString();
    
            List<StoreBrands> list = new List<StoreBrands>();
            foreach(var d in getbrands)
            {
                
                var x = new StoreBrands();
                x.BrandId = d.BrandId.ToString();
                x.StoreId = d.StoreId.ToString() ;
                list.Add(x);
            }
            n.storeBrands = list;
            return Json(new { n });
        }
        public IActionResult Profile(string id)
        {
            var item = db.AspNetUsers.FirstOrDefault(x => x.Id == id);
            var getDetail = db.TblUserDetail.FirstOrDefault(x => x.UserId == item.Id);
            var getstore = db.TblStore.FirstOrDefault(x => x.UserId == item.Id);
            var n = new UserViewModel();
            n.Id = item.Id;
            n.Email = item.Email;
            n.PhoneNumber = item.PhoneNumber;
            n.UserName = item.UserName;
            
            if (getDetail != null)
            {
                var getBank = db.TblBankDetail.FirstOrDefault(x => x.Id == getDetail.BankId);
                var getAddress = db.TblAddress.FirstOrDefault(x => x.Id == getDetail.AddressId);
                n.FullName = getDetail.FullName;
                n.Cnic = getDetail.Cnic;
                n.DOB = Convert.ToDateTime(getDetail.Dob).ToString("dd MMMM yyyy");
                n.ProfilePicture = getDetail.ProfilePicture;
                n.About = getDetail.About;
                if(getAddress!=null)
                {
                    var country = db.TblCountry.FirstOrDefault(x => x.Id == getAddress.CountryId);
                    var region = db.TblRegion.FirstOrDefault(x => x.Id == getAddress.RegionId);
                    var city = db.TblCity.FirstOrDefault(x => x.Id == getAddress.CityId);
                    var area = db.TblArea.FirstOrDefault(x => x.Id == getAddress.AreaId);

                   
                    n.LocationName = getAddress.LocationName;
                    n.AddressDescription = getAddress.AddressDescription;
                    n.Country = country.CountryName;
                    n.Region = region.RegionName;
                    n.City = city.CityName;
                  
                }
                if (getBank != null)
                {
                    n.AccountTitle = getBank.AccountTitle;
                    n.AccountNo = getBank.AccountNo;
                    n.BranchCode = getBank.BranchCode;
                    n.ChequeImage = getBank.ChequeImage;
                 
                }
                

            }
            if (getstore == null)
            {
                n.UserType = "Buyer";
                n.StoreId = "";
            }
            else
            {
                var getCategory = db.TblCategory.FirstOrDefault(x => x.Id == getstore.CategoryId);
                var getbank = db.TblBankDetail.FirstOrDefault(x => x.Id == getstore.BankId);
                var getnatureofBusiness = db.TblNatureOfBusiness.FirstOrDefault(x => x.Id == getstore.NatureOfBusinessId);
                var getAddress = db.TblAddress.FirstOrDefault(x => x.Id == getstore.AddressId);
                var country = db.TblCountry.FirstOrDefault(x => x.Id == getAddress.CountryId);
                var region = db.TblRegion.FirstOrDefault(x => x.Id == getAddress.RegionId);
                var city = db.TblCity.FirstOrDefault(x => x.Id == getAddress.CityId);
                var area = db.TblArea.FirstOrDefault(x => x.Id == getAddress.AreaId);
                n.UserType = "Seller";
                n.StoreId = getstore.Id.ToString();
                n.CompanyName = getstore.CompanyName;
                n.CompanyLogo = getstore.CompanyLogo;
                n.CompanyLicence = getstore.CompanyLicence;
                if(getstore.CreatedOn!=null)
                {
                    n.StoreCreatedOn = Convert.ToDateTime(getstore.CreatedOn).ToString("dd/MMM/yyyy");

                }
                else
                {
                    n.StoreCreatedOn = "";

                }
                n.StoreCountry = country.CountryName;
                n.StoreCity = city.CityName;
                n.StoreRegion = region.RegionName;
                n.StoreArea = area.AreaName;
                n.StoreAddress = getAddress.AddressDescription;
                if(getAddress.Longitude!=null)
                {
                    n.StoreLongitude = getAddress.Longitude.ToString();
                }
                if(getAddress.Latitude!=null)
                {
                    n.StoreLatitude = getAddress.Latitude.ToString();

                }

                n.Category = getCategory.CategoryName;
                n.StoreBankAccountTitle = getbank.AccountTitle;
                n.StoreAccountNumber = getbank.AccountNo;
                n.StoreBranchCode = getbank.BranchCode;
                n.StoreChequeImage = getbank.ChequeImage;
                n.LicenceFile = getstore.LicenceFile;
                if(getstore.CompanyRegDate!=null)
                {
                    n.CompanyRegDate = Convert.ToDateTime(getstore.CompanyRegDate).ToString("dd/MMM/yyyy");

                }
                else
                {
                    n.CompanyRegDate = "";
                }
                n.NatureOfBusiness = getnatureofBusiness.NatureOfBusiness;
                n.NTNNumber = getstore.Ntnnumber;
                n.BusinessAddress = getstore.BusinessAddress;
                n.ReturnAddress = getstore.ReturnAddress;
                if(n.NoOfProductYouListing!=null)
                {
                    n.NoOfProductYouListing = getstore.NoOfProductYouListing.ToString();

                }
                else
                {
                    n.NoOfProductYouListing = "0";

                }

                n.AreYouSallingOnOtherWebsite =Convert.ToBoolean( getstore.AreYouSallingOnOtherWebsite);
               
            }
            return View(n);
        }
        public async Task<string> SaveUserDetail(string Id, string UserID, string FullName, string DOB, string BankAccountTitle, string AccountNumber, string BranchCode, string Country,
            string Region, string City, string Area, string Address, string AboutUser, string Image, string ChequeImage,string cnic)
        {
            string res="";
            try
            {
                if(Id=="0")
                {
                    var a = new TblAddress();
                    if(Country!="")
                    {
                        a.CountryId = Convert.ToDecimal(Country);

                    }
                    if (Region != "")
                    {
                        a.RegionId = Convert.ToDecimal(Region);

                    }
                    if (City != "")
                    {
                        a.CityId = Convert.ToDecimal(City);

                    }
                    if (Area != "")
                    {
                        a.AreaId = Convert.ToDecimal(Area);

                    }
               
                    a.AddressDescription = Address;
                    await db.TblAddress.AddAsync(a);
                    await db.SaveChangesAsync();
                    var b = new TblBankDetail();
                    b.AccountTitle = BankAccountTitle;
                    b.AccountNo = AccountNumber;
                    b.BranchCode = BranchCode;
                    b.ChequeImage = ChequeImage;
                    b.UserId = UserID;
                    await db.TblBankDetail.AddAsync(b);
                    await db.SaveChangesAsync();
                    var t =new TblUserDetail();
                    t.FullName = FullName;
                    t.CreatedOn = DateTime.Now;
                    t.Dob = Convert.ToDateTime(DOB);
                    t.UserId = UserID;
                    t.ProfilePicture = Image;
                    t.BankId = b.Id;
                    t.Cnic = cnic;
                    t.AddressId = a.Id;
                    t.About = AboutUser;
                    await db.TblUserDetail.AddAsync(t);
                    await db.SaveChangesAsync();
                    res = "true";
                }
                else
                {
                    var UID = Convert.ToDecimal(Id);
                   
                    var t = db.TblUserDetail.FirstOrDefault(x=>x.Id==UID);
                    t.FullName = FullName;
                 
                    t.Dob = Convert.ToDateTime(DOB);
                    t.UserId = UserID;
                    t.ProfilePicture = Image;
                    t.AddressId = t.AddressId;
                    t.BankId = t.BankId;
                    t.Cnic = cnic;
                   
                    t.About = AboutUser;
                     db.TblUserDetail.Update(t);
                    await db.SaveChangesAsync();
                    var a = db.TblAddress.FirstOrDefault(x => x.Id == t.AddressId);
                    if (Country != "")
                    {
                        a.CountryId = Convert.ToDecimal(Country);

                    }
                    if (Region != "")
                    {
                        a.RegionId = Convert.ToDecimal(Region);

                    }
                    if (City != "")
                    {
                        a.CityId = Convert.ToDecimal(City);

                    }
                    if (Area != "")
                    {
                        a.AreaId = Convert.ToDecimal(Area);

                    }
                    a.AddressDescription = Address;
                     db.TblAddress.Update(a);
                    await db.SaveChangesAsync();
                    var b = db.TblBankDetail.FirstOrDefault(x=>x.Id==t.BankId);
                    b.AccountTitle = BankAccountTitle;
                    b.AccountNo = AccountNumber;
                    b.BranchCode = BranchCode;
                    b.ChequeImage = ChequeImage;
                    b.UserId = UserID;
                    db.TblBankDetail.Update(b);
                    await db.SaveChangesAsync();
                    res = "updated";
                }
               
            }
            catch(Exception ex)
            {
                res= ex.Message;
            }
            return res;
        }
        public IActionResult EditProduct(decimal Id)
        {
            var item = db.TblProduct.FirstOrDefault(x => x.Id == Id);
            var n = new ProductViewModel();
            n.Id = item.Id.ToString();
            n.UserId = item.UserId;
            if(item.ProductUnitId!=null)
            {
                n.UOMId = item.ProductUnitId.ToString();
            }
            else
            {
                n.UOMId = "";
            }
            if(item.SecondLevelCategoryId!=null)
            {
                n.SecondCategory = item.SecondLevelCategoryId.ToString();
            }
            else
            {
                n.SecondCategory = "";
            }
            if (item.ThirdLevelCategoryId != null)
            {
                n.ThirdCategory = item.ThirdLevelCategoryId.ToString();
            }
            else
            {
                n.ThirdCategory = "";
            }
            if (item.ForthLevelCategoryId != null)
            {
                n.FourthCategory = item.ForthLevelCategoryId.ToString();
            }
            else
            {
                n.FourthCategory = "";
            }
            if (item.SecondLevelCategoryId != null)
            {
                n.SecondCategory = item.SecondLevelCategoryId.ToString();
            }
            else
            {
                n.SecondCategory = "";
            }
            n.ProductName = item.ProductName;
            n.ProductNameInEnglish = item.ProductNameInEnglish;
            n.BrandId = item.BrandId.ToString();
            n.CategoryId = item.CategoryId.ToString();
            n.StoreID = item.StoreId.ToString();
            n.Highlights = item.Highlights;
            n.ProductDescription = item.ProductDescription;
            n.EnglishHighlights = item.EnglishHighlights;
            n.WarrantyType = item.WarrantyType;
            n.WhatInBox = item.WhatInBox; 
            if (item.PackageWeghtInKg != null)
            {
                n.PackageWeghtInKg = item.PackageWeghtInKg.ToString();
            }
            if (item.Length != null)
            {
                n.Length = item.Length.ToString();
            }
            if (item.Width != null)
            {
                n.Width = item.Width.ToString();
            }
            if (item.Height != null)
            {
                n.Height = item.Height.ToString();
            }
          
            n.ColorFamily = item.ColorFamily;
            n.SellerSKU = item.SellerSku;
            if(item.TotalQuantity!=null)
            {
                n.TotalQuantity = item.TotalQuantity.ToString();
            }
            if (item.AvailableQuantity != null)
            {
                n.AvailableQuantity = item.AvailableQuantity.ToString();
            }

            if (item.Price != null)
            {
                n.Price = item.Price.ToString();
            }
            if (item.SpecialPrice != null)
            {
                n.SpecialPrice = item.SpecialPrice.ToString();
            }

            if(item.IsFront==true)
            {
                n.IsFront = item.IsFront.ToString();
            }

            
            n.IThumbNailImaged = item.ThumbNailImage;
            n.IsNew = item.IsNew.ToString();
            n.IsFeatured = item.IsFeatured.ToString();
            n.IsBest = item.IsBest.ToString();
            n.IsSale = item.IsSale.ToString();
            n.IsSpecial = item.IsSpecial.ToString();
            return Json(new { n });
        }
        public async Task<string> SaveProduct(string Id, string UserID, string StoreID, string BrandId, string CategoryId,
            string SecondCategory, string ThirdCategory, string FourthCategory,
            string ProductName
            , string ProductNameInEnglish, string Highlights,string HighlightsInEnglish,string WarrantyType,string WhatsInBox,
            string PackageWeightInKG, string PackageLength, string PackageWidth, string PackhageHeight, string ColorFamily
            , string SKU, string TotalQuantity, string UnitPrice, string SpecialPrice, string IsNew, string IsFeatured, string IsBest,
            string IsSpecial, string IsSale, string Thumbnail, string DescriptionProduct,string UnitID,string IsFront)
        {
            string res = "";
            try
            {
                if(Id=="0")
                {
                    var n = new TblProduct();
                    n.UserId = UserID;
                    n.StoreId = Convert.ToDecimal(StoreID);
                    n.BrandId = Convert.ToDecimal(BrandId);
                    n.CategoryId = Convert.ToDecimal(CategoryId);
                    n.ProductName = ProductName;
                    n.ProductNameInEnglish = ProductNameInEnglish;
                    n.Highlights = Highlights;
                    n.EnglishHighlights = HighlightsInEnglish;
                    n.WarrantyType = WarrantyType;
                    n.WhatInBox = WhatsInBox;
                    if (SecondCategory != "" && SecondCategory != null)
                    {
                        n.SecondLevelCategoryId = Convert.ToDecimal(SecondCategory);
                    }
                    if (ThirdCategory != "" && ThirdCategory != null)
                    {
                        n.ThirdLevelCategoryId = Convert.ToDecimal(ThirdCategory);
                    }
                    if (FourthCategory != "" && FourthCategory != null)
                    {
                        n.ForthLevelCategoryId = Convert.ToDecimal(FourthCategory);
                    }

                    try
                    {
                        n.ProductUnitId = Convert.ToDecimal(UnitID);
                    }
                    catch(Exception ex)
                    {

                    }
                    n.CreatedOn = DateTime.Now;
                    if (PackageWeightInKG != "" && PackageWeightInKG != null)
                    {
                        n.PackageWeghtInKg = Convert.ToDecimal(PackageWeightInKG);
                    }
                    if (PackageWidth != "" && PackageWidth != null)
                    {
                        n.Width = Convert.ToDecimal(PackageWidth);
                    }
                    if (PackhageHeight != "" && PackhageHeight != null)
                    {
                        n.Height = Convert.ToDecimal(PackhageHeight);
                    }
                    if (PackageLength != "" && PackageLength != null)
                    {
                        n.Length = Convert.ToDecimal(PackageLength);
                    }
                    n.ColorFamily = ColorFamily;
                    n.SellerSku = SKU;
                    if (TotalQuantity != "" && TotalQuantity != null)
                    {
                        n.TotalQuantity = Convert.ToDecimal(TotalQuantity);
                        n.AvailableQuantity = Convert.ToDecimal(TotalQuantity);
                    }
                    if (UnitPrice != "" && UnitPrice != null)
                    {
                        n.Price = Convert.ToDecimal(UnitPrice);

                    }
                    if (SpecialPrice != "" && SpecialPrice != null)
                    {
                        n.SpecialPrice = Convert.ToDecimal(SpecialPrice);

                    }
                    if (IsNew == "on")
                    {
                        n.IsNew = true;
                    }
                    else
                    {
                        n.IsNew = false;
                    }
                    if (IsFront == "on")
                    {
                        n.IsFront = true;
                    }
                    else
                    {
                        n.IsFront = false;
                    }
                    if (IsFeatured == "on")
                    {
                        n.IsFeatured = true;
                    }
                    else
                    {
                        n.IsFeatured = false;
                    }
                    if (IsBest == "on")
                    {
                        n.IsBest = true;
                    }
                    else
                    {
                        n.IsBest = false;
                    }
                    if (IsSpecial == "on")
                    {
                        n.IsSpecial = true;
                    }
                    else
                    {
                        n.IsSpecial = false;
                    }
                    if (IsSale == "on")
                    {
                        n.IsSale = true;
                    }
                    else
                    {
                        n.IsSale = false;
                    }
                    n.Status = "Approved";
                    n.ThumbNailImage = Thumbnail;
                    n.ProductDescription = DescriptionProduct;
                    await db.TblProduct.AddAsync(n);
                    await db.SaveChangesAsync();
                    res = "true";
                }
                else
                {
                    var PId = Convert.ToDecimal(Id);
                    var n = db.TblProduct.FirstOrDefault(x=>x.Id== PId);
                    var getDIfferOfProduct = Convert.ToDecimal(TotalQuantity)- n.AvailableQuantity ;
                    n.UserId = UserID;
                    n.StoreId = Convert.ToDecimal(StoreID);
                    n.BrandId = Convert.ToDecimal(BrandId);
                    n.CategoryId = Convert.ToDecimal(CategoryId);
                    n.ProductName = ProductName;
                    n.ProductNameInEnglish = ProductNameInEnglish;
                    n.Highlights = Highlights;
                    n.EnglishHighlights = HighlightsInEnglish;
                    n.WarrantyType = WarrantyType;
                    n.WhatInBox = WhatsInBox;
                    if (SecondCategory != ""&&SecondCategory!=null)
                    {
                        n.SecondLevelCategoryId = Convert.ToDecimal(SecondCategory);
                    }
                    if (ThirdCategory != "" && ThirdCategory != null)
                    {
                        n.ThirdLevelCategoryId = Convert.ToDecimal(ThirdCategory);
                    }
                    if (FourthCategory != "" && FourthCategory != null)
                    {
                        n.ForthLevelCategoryId = Convert.ToDecimal(FourthCategory);
                    }
                    try
                    {
                        n.ProductUnitId = Convert.ToDecimal(UnitID);
                    }
                    catch (Exception ex)
                    {

                    }
                    n.CreatedOn = DateTime.Now;
                    if (PackageWeightInKG != "" && PackageWeightInKG != null)
                    {
                        n.PackageWeghtInKg = Convert.ToDecimal(PackageWeightInKG);
                    }
                    if (PackageWidth != "" && PackageWidth != null)
                    {
                        n.Width = Convert.ToDecimal(PackageWidth);
                    }
                    if (PackhageHeight != "" && PackhageHeight != null)
                    {
                        n.Height = Convert.ToDecimal(PackhageHeight);
                    }
                    if (PackageLength != "" && PackageLength != null)
                    {
                        n.Length = Convert.ToDecimal(PackageLength);
                    }
                    n.ColorFamily = ColorFamily;
                    n.SellerSku = SKU;
                    if (TotalQuantity != "" && TotalQuantity != null)
                    {
                        n.TotalQuantity = n.TotalQuantity+Convert.ToDecimal(getDIfferOfProduct);
                        n.AvailableQuantity = n.AvailableQuantity + Convert.ToDecimal(getDIfferOfProduct);
                    }
                    if (UnitPrice != "" && UnitPrice != null)
                    {
                        n.Price = Convert.ToDecimal(UnitPrice);

                    }
                    if (SpecialPrice != "" && SpecialPrice != null)
                    {
                        n.SpecialPrice = Convert.ToDecimal(SpecialPrice);

                    }
                    if (IsNew == "on")
                    {
                        n.IsNew = true;
                    }
                    else
                    {
                        n.IsNew = false;
                    }
                    if (IsFeatured == "on")
                    {
                        n.IsFeatured = true;
                    }
                    else
                    {
                        n.IsFeatured = false;
                    }
                    if (IsFront == "on")
                    {
                        n.IsFront = true;
                    }
                    else
                    {
                        n.IsFront = false;
                    }
                    if (IsBest == "on")
                    {
                        n.IsBest = true;
                    }
                    else
                    {
                        n.IsBest = false;
                    }
                    if (IsSpecial == "on")
                    {
                        n.IsSpecial = true;
                    }
                    else
                    {
                        n.IsSpecial = false;
                    }
                    if (IsSale == "on")
                    {
                        n.IsSale = true;
                    }
                    else
                    {
                        n.IsSale = false;
                    }
                    n.Status = "Approved";
                    n.ThumbNailImage = Thumbnail;
                    n.ProductDescription = DescriptionProduct;
                    db.TblProduct.Update(n);
                    await db.SaveChangesAsync();
                    res = "updated";
                }
                
            }
            catch(Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }


        public async Task<string> SaveStore(string Id, string CompanyAddress, string Area, string City, string Region, 
            string Country, string BranchCode, string AccountNumber,string CompanyLicenceFile,
            string ChequeImage, string Image, string termsAndCondition, string ReturnAddress, 
            string Category, string ProductUlisting, string CompanyNTNNo, string CompanyRegtrationDate, 
            string CompanyRegNo, string CompanyLicence, string CompanyName, string UserID,
            string BankAccountTitle, string Brands,string NatureOfBusiness)
        {
            string res = "";
            try
            {
                if(Id=="0")
                {
                    var jsonStr = JsonSerializer.Deserialize<List<string>>(Brands);

                    var s = new TblStore();
                    s.UserId = UserID;
                    //s.BankId = b.Id;
                    //s.AddressId = a.Id;
                    s.CategoryId = Convert.ToDecimal(Category);
                    s.CompanyName = CompanyName;
                    s.CompanyLogo = Image;
                    s.CompanyLicence = CompanyLicence;
                    s.CreatedOn = DateTime.Now;
                    s.CompanyRegDate = Convert.ToDateTime(CompanyRegtrationDate);
                    s.Ntnnumber = CompanyNTNNo;
                    s.BusinessAddress = CompanyAddress;
                    s.ReturnAddress = ReturnAddress;
                    s.CompanyRegNo = CompanyRegNo;
                    s.LicenceFile = CompanyLicenceFile;
                    if (termsAndCondition == "on")
                    {
                        s.TermsAndConditions = true;
                    }
                    else
                    {
                        s.TermsAndConditions = false;
                    }

                    s.NoOfProductYouListing = Convert.ToDecimal(ProductUlisting);
                    s.NatureOfBusinessId = Convert.ToDecimal(NatureOfBusiness);
                    db.TblStore.Add(s);
                    await db.SaveChangesAsync();
                    var a = new TblAddress();
                  
                    a.CountryId = Convert.ToDecimal(Country);
                    a.RegionId = Convert.ToDecimal(Region);
                    a.CityId = Convert.ToDecimal(City);
                    if (Area != ""&&Area!=null)
                    {
                        a.AreaId = Convert.ToDecimal(Area);
                    }
                    a.StoreId = s.Id;
                    a.AddressDescription = CompanyAddress;
                    await db.TblAddress.AddAsync(a);
                    await db.SaveChangesAsync();
                    var b = new TblBankDetail();
                    b.AccountTitle = BankAccountTitle;
                    b.AccountNo = AccountNumber;
                    b.BranchCode = BranchCode;
                    b.StoreId = s.Id;
                    b.ChequeImage = ChequeImage;
                    b.UserId = UserID;
                    await db.TblBankDetail.AddAsync(b);
                    await db.SaveChangesAsync();
                    var brands = Brands.Split(',');
                    foreach (var item in jsonStr)
                    {
                        if (item != "")
                        {
                            var br = new TblStoreBrand();
                            br.StoreId = s.Id;
                            br.BrandId = Convert.ToDecimal(item);
                            db.TblStoreBrand.Add(br);
                        }

                    }
                    await db.SaveChangesAsync();

                    res = "true";
                }
                else
                {
                    var SId = Convert.ToDecimal(Id);
                    var s = db.TblStore.FirstOrDefault(x => x.Id == SId);

                    var a = db.TblAddress.FirstOrDefault(x=>x.StoreId==s.Id);
                  
                    a.CountryId = Convert.ToDecimal(Country);
                    a.RegionId = Convert.ToDecimal(Region);
                    a.CityId = Convert.ToDecimal(City);
                    if (Area != ""&&Area!=null)
                    {
                        a.AreaId = Convert.ToDecimal(Area);
                    }

                    a.AddressDescription = CompanyAddress;
                    db.TblAddress.Update(a);
                    await db.SaveChangesAsync();
                    var b = db.TblBankDetail.FirstOrDefault(x=>x.StoreId==s.Id);
                    b.AccountTitle = BankAccountTitle;
                    b.AccountNo = AccountNumber;
                    b.BranchCode = BranchCode;
                    b.ChequeImage = ChequeImage;
                    b.UserId = UserID;
                    db.TblBankDetail.Update(b);
                    await db.SaveChangesAsync();
                  
                    s.UserId = UserID;
                    s.BankId = b.Id;
                    s.AddressId = a.Id;
                    s.CategoryId = Convert.ToDecimal(Category);
                    s.CompanyName = CompanyName;
                    s.CompanyLogo = Image;
                    s.CompanyLicence = CompanyLicence;
                    s.CreatedOn = DateTime.Now;
                    s.CompanyRegDate = Convert.ToDateTime(CompanyRegtrationDate);
                    s.Ntnnumber = CompanyNTNNo;
                    s.BusinessAddress = CompanyAddress;
                    s.ReturnAddress = ReturnAddress;
                    s.CompanyRegNo = CompanyRegNo;
                    s.LicenceFile = CompanyLicenceFile;
                    if (termsAndCondition == "on")
                    {
                        s.TermsAndConditions = true;
                    }
                    else
                    {
                        s.TermsAndConditions = false;
                    }

                    s.NoOfProductYouListing = Convert.ToDecimal(ProductUlisting);
                    s.NatureOfBusinessId = Convert.ToDecimal(NatureOfBusiness);
                    db.TblStore.Update(s);
                    await db.SaveChangesAsync();
                    db.TblStoreBrand.RemoveRange(db.TblStoreBrand.Where(x => x.StoreId == s.Id));
                    var jsonStr = JsonSerializer.Deserialize<List<string>>(Brands);

                    foreach (var item in jsonStr)
                    {
                        if (item != "")
                        {
                            var br = new TblStoreBrand();
                            br.StoreId = s.Id;
                            br.BrandId = Convert.ToDecimal(item);
                            db.TblStoreBrand.Add(br);
                        }
                    }
                    await db.SaveChangesAsync();

                    res = "updated";
                }
               
            }
            catch (Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }
        public IActionResult GetUserDetail(string UserID)
        { 
            var n = new UserViewModel();
            var item = db.AspNetUsers.FirstOrDefault(x => x.Id == UserID);

            var getDetail = db.TblUserDetail.FirstOrDefault(x => x.UserId == item.Id);
            n.Email = item.Email;
            n.PhoneNumber = item.PhoneNumber;
            n.UserName = item.UserName;
            n.UserId = item.Id;
            if (getDetail != null)
            {
                n.Id = getDetail.Id.ToString();
                var getBank = db.TblBankDetail.FirstOrDefault(x => x.Id == getDetail.BankId);
                var getAddress = db.TblAddress.FirstOrDefault(x => x.Id == getDetail.AddressId);
                n.FullName = getDetail.FullName;
                n.Cnic = getDetail.Cnic;
                n.DOB = Convert.ToDateTime(getDetail.Dob).ToString("MMM/dd/yyyy");
                n.ProfilePicture = getDetail.ProfilePicture;
                n.About = getDetail.About;
                if(getBank!=null)
                {
                    n.AccountTitle = getBank.AccountTitle;
                    n.AccountNo = getBank.AccountNo;
                    n.BranchCode = getBank.BranchCode;
                    n.ChequeImage = getBank.ChequeImage;
                }
                if (getAddress != null)
                {

                   
                    if(getAddress.CountryId!=null)
                    {
                        var country = db.TblCountry.FirstOrDefault(x => x.Id == getAddress.CountryId);
                        n.Country = country.Id.ToString();
                    }
                    if (getAddress.RegionId != null)
                    {
                        var region = db.TblRegion.FirstOrDefault(x => x.Id == getAddress.RegionId);
                        n.Region = region.Id.ToString();
                    }
                    if (getAddress.CityId != null)
                    {
                        var city = db.TblCity.FirstOrDefault(x => x.Id == getAddress.CityId);
                        n.City = city.Id.ToString();
                    }

                    if (getAddress.CityId != null)
                    {
                        var area = db.TblArea.FirstOrDefault(x => x.Id == getAddress.AreaId);

                        n.Area = area.Id.ToString();
                    }

                 

                    n.LocationName = getAddress.LocationName;
                    n.AddressDescription = getAddress.AddressDescription;
                }
            }
            else
            {
                n.Id = "0";
            }
            return Json(new { res = n });
        }
        public async Task<JsonResult> SaveProfilePicture(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);
            var UniqueName = "";
            foreach (var formFile in files)
            {
                try
                {
                    if (formFile.Length > 0)
                    {
                        string UploadsFolder = Path.Combine(path, "Images\\ProfileImages");
                        if (!Directory.Exists(UploadsFolder))
                        {
                            Directory.CreateDirectory(UploadsFolder);
                        }
                        UniqueName = Guid.NewGuid() + "_" + formFile.FileName;
                        string filePath = Path.Combine(UploadsFolder, UniqueName);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(stream);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Json("");
                }

            }
            return Json("Images/ProfileImages/"+UniqueName);

        }
        public async Task<JsonResult> SaveCompanyLogo(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);
            var UniqueName = "";
            foreach (var formFile in files)
            {

                try
                {
                    if (formFile.Length > 0)
                    {
                        string UploadsFolder = Path.Combine(path, "Images\\CompanyImages");
                        if (!Directory.Exists(UploadsFolder))
                        {
                            Directory.CreateDirectory(UploadsFolder);
                        }
                        UniqueName = Guid.NewGuid() + "_" + formFile.FileName;
                        string filePath = Path.Combine(UploadsFolder, UniqueName);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(stream);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Json("");
                }

            }
            return Json("Images/CompanyImages/"+UniqueName);

        }
        public async Task<JsonResult> SaveChequeImage(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);
            var UniqueName = "";
            foreach (var formFile in files)
            {

                try
                {
                    if (formFile.Length > 0)
                    {
                        string UploadsFolder = Path.Combine(path, "Images\\ChequeImage");
                        if (!Directory.Exists(UploadsFolder))
                        {
                            Directory.CreateDirectory(UploadsFolder);
                        }
                        UniqueName = Guid.NewGuid() + "_" + formFile.FileName;
                        string filePath = Path.Combine(UploadsFolder, UniqueName);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(stream);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Json("");
                }

            }
            return Json("Images/ChequeImage/"+UniqueName);

        }
       public string GetallStoreDRP(string UserID)
        {
            var html = "<option value=''>--Select Store--</option>";
            var getallStore = db.TblStore.Where(x => x.UserId == UserID);
            foreach(var item in getallStore)
            {
                html = html + "<option value=" + item.Id + "> " + item.CompanyName + " </option>";
            }
            return html;
        }
        public string GetallStoreProductDRP(string StoreID)
        {
            var html = "<option value=''>--Select Product--</option>";
            try
            {
                decimal Id = Convert.ToDecimal(StoreID);
             
                var getallStore = db.TblProduct.Where(x => x.StoreId == Id);
                foreach (var item in getallStore)
                {
                    html = html + "<option value=" + item.Id + "> " + item.ProductName + " </option>";
                }
            }
            catch(Exception ex)
            {
                html = "<option value=''>--Select Product--</option>";
            }
           
            return html;
        }
        [HttpPost]
        public async Task<JsonResult> SaveProductImages()
        {
            var files2 = HttpContext.Request.Form.Files;
            var s=HttpContext.Request.Form["Product"].ToString();
            var t = s.Split(',')[0];
            //long size = files.Sum(f => f.Length);
            var UniqueName = "";
            string res = "";
            foreach (var formFile in files2)
            {

                try
                {
                    if (formFile.Length > 0)
                    {
                        string UploadsFolder = Path.Combine(path, "Images\\Products");
                        if (!Directory.Exists(UploadsFolder))
                        {
                            Directory.CreateDirectory(UploadsFolder);
                        }
                        UniqueName = Guid.NewGuid() + "_" + formFile.FileName;
                        string filePath = Path.Combine(UploadsFolder, UniqueName);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(stream);
                        }
                        var n = new TblProductImages();
                        n.ImagePath = "Images/Products/"+UniqueName;
                        n.ProductId =Convert.ToDecimal(t);
                        db.TblProductImages.Add(n);
                        await db.SaveChangesAsync();
                        res = "true";
                    }
                }
                catch (Exception ex)
                {
                    res = ex.Message;
                }

            }
           
            return Json("Images/Products/"+res);

        }
        public async Task<JsonResult> SaveProductThumbnails(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);
            var UniqueName = "";
            foreach (var formFile in files)
            {

                try
                {
                    if (formFile.Length > 0)
                    {
                        string UploadsFolder = Path.Combine(path, "Images\\Products");
                        if (!Directory.Exists(UploadsFolder))
                        {
                            Directory.CreateDirectory(UploadsFolder);
                        }
                        UniqueName = Guid.NewGuid() + "_" + formFile.FileName;
                        string filePath = Path.Combine(UploadsFolder, UniqueName);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(stream);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Json("");
                }

            }
            return Json("Images/Products/"+UniqueName);

        }
        public async Task<JsonResult> SaveCompanyLicenceFile(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);
            var UniqueName = "";
            foreach (var formFile in files)
            {

                try
                {
                    if (formFile.Length > 0)
                    {
                        string UploadsFolder = Path.Combine(path, "Images\\CompanyLicenceFile");
                        if (!Directory.Exists(UploadsFolder))
                        {
                            Directory.CreateDirectory(UploadsFolder);
                        }
                        UniqueName = Guid.NewGuid() + "_" + formFile.FileName;
                        string filePath = Path.Combine(UploadsFolder, UniqueName);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(stream);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Json("");
                }

            }
            return Json("Images/CompanyLicenceFile/"+UniqueName);

        }
    }
}