﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminStore.Services
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Date { get; set; }
        public string FullName { get; set; }
        public string DOB { get; set; }
        public string mobile { get; set; }
        public string ProfilePicture { get; set; }
        public string About { get; set; }
        public string Cnic { get; set; }
        public string LocationName { get; set; }
        public string AddressDescription { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public string Area { get; set; }
     
        public string AccountTitle { get; set; }
        public string AccountNo { get; set; }
        public string BranchCode { get; set; }
        public string ChequeImage { get; set; }
        //Store Data
       public string StoreId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyLogo { get; set; }
        public string CompanyLicence { get; set; }
        public string StoreCountry { get; set; }
        public string StoreCity { get; set; }
        public string StoreRegion { get; set; }
        public string StoreArea { get; set; }
        public string StoreAddress { get; set; }
        public string StoreLongitude { get; set; }
        public string StoreLatitude { get; set; }
        public string Category { get; set; }
        public string StoreBankAccountTitle { get; set; }
        public string StoreAccountNumber { get; set; }
        public string StoreBranchCode { get; set; }
        public string StoreChequeImage { get; set; }
        public string LicenceFile { get; set; }
        public string StoreCreatedOn { get; set; }
        public string CompanyRegDate { get; set; }
        public string NatureOfBusiness { get; set; }
        public string NTNNumber { get; set; }
        public string BusinessAddress { get; set; }
        public string ReturnAddress { get; set; }
        public string NoOfProductYouListing { get; set; }
        public Boolean AreYouSallingOnOtherWebsite { get; set; }
    }
}
