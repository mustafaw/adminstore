﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminStore.Services
{
    public class ProductViewModel
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string ProductName { get; set; }
        public string ProductNameInEnglish { get; set; }
        public string BrandId { get; set; }
        public string CategoryId { get; set; }
        public string SecondCategory { get; set; }
        public string ThirdCategory { get; set; }
        public string FourthCategory { get; set; }
        public string UOMId { get; set; }
        public string StoreID { get; set; }
        public string Highlights { get; set; }
        public string ProductDescription { get; set; }
        public string EnglishHighlights { get; set; }
        public string WarrantyType { get; set; }
        public string WhatInBox { get; set; }
        public string PackageWeghtInKg { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string ColorFamily { get; set; }
        public string SellerSKU { get; set; }
        public string TotalQuantity { get; set; }
        public string AvailableQuantity { get; set; }
        public string Price { get; set; }
        public string SpecialPrice { get; set; }
        public string IThumbNailImaged { get; set; }
        public string IsNew { get; set; }
        public string IsFeatured { get; set; }
        public string IsBest  { get; set; }
        public string IsSale { get; set; }
        public string IsFront { get; set; }
        public string IsSpecial { get; set; }
    }
    public class ProductImages
    {
        public string Id { get; set; }

        public string CreateOn { get; set; }
        public string storeName { get; set; }
        public string UserName { get; set; }
        public string Thumbnails { get; set; }
        public string Category { get; set; }
        public string Brand { get; set; }
        public string ProductName { get; set; }
        public string ProductId { get; set; }
    }
}
