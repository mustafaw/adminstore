﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminStore.Services
{
    public class IdentityService
    {
        private readonly RoleManager<IdentityRole> roleManger;
        private readonly UserManager<IdentityUser> UserManger;

        public IdentityService(UserManager<IdentityUser> userMange, RoleManager<IdentityRole> RoleManger)
        {
            this.roleManger = RoleManger;
            this.UserManger = userMange;
        }
        //Create role
        public async Task<bool> CreateRole(string RoleName)
        {
            IdentityRole identityRole = new IdentityRole
            {
                Name = RoleName
            };

            IdentityResult result = await roleManger.CreateAsync(identityRole);
            if (result.Succeeded)
            {
                return true;
            }
            else
            {
                ///For Checking an error
                foreach (IdentityError error in result.Errors)
                {
                    var t = error.Description;
                }
                return false;
            }

        }

        //Getall Roles
        public IQueryable<IdentityRole> ListRoles()
        {
            var roles = roleManger.Roles;
            return roles;
        }
        //Edit Role
        public async Task<string> EditRole(string Id, string Name)
        {
            var role = await roleManger.FindByIdAsync(Id);
            if (role == null)

            {
                return "RoleNotFound";
            }
            else
            {
                role.Name = Name;
                var result = await roleManger.UpdateAsync(role);
                if (result.Succeeded)
                {
                    return "true";
                }
                else
                {
                    return "false";
                }

            }
        }
        public async Task<string> RemoveTask(string UserId)
        {
            var User = await UserManger.FindByIdAsync(UserId);
            var GetallRoles = roleManger.Roles.ToList();
            foreach (var item in GetallRoles)
            {
                var result = await UserManger.RemoveFromRoleAsync(User, item.Name);
            }
            return "true";
        }
        //Assign Role To User
        public async Task<string> AssignRoleToUser(string UserId, String RoleName)
        {

            try
            {
                var User = await UserManger.FindByIdAsync(UserId);
                var GetallRoles = roleManger.Roles;
                //Remove User From all Roles
                //foreach (var item in GetallRoles)
                //{
                //    var result = await UserManger.RemoveFromRoleAsync(User, item.Name);
                //}
                var role = await roleManger.FindByNameAsync(RoleName);
                if (role != null)
                {

                    //AssignRole
                    if (!(await UserManger.IsInRoleAsync(User, role.Name)))
                    {
                        var result = await UserManger.AddToRoleAsync(User, role.Name);
                        if (result.Succeeded)
                        {
                            return "true";
                        }
                        else
                        {
                            return "false";
                        }
                    }
                    else
                    {
                        return "true";
                    }
                }
                else
                {
                    return "roleNotFound";
                }
            }
            catch (Exception ex)
            {
                return "false";
            }


        }
    }
}
