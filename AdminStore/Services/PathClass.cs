﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AdminStore.Services
{
    public class PathClass
    {
     
        public string getpthString()
        {
            var config = GetConfiguration();
            var con = config.GetSection("ConnectionStrings").GetSection("FilesPath").Value.ToString();
            return con.ToString();
        }
        public string GetDomainNme()
        {
            var domainName = "https://winstore.pk/";
            return domainName;

        }
        public IConfigurationRoot GetConfiguration()
        {

            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            return builder.Build();
        }
    }
}
