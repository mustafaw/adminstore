﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminStore.Services
{
    public class StoreViewModel
    {
        public string Id { get; set; }
        public string userId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyLogo { get; set; }
        public string CompanyLicence { get; set; }
        public string AddressId { get; set; }
        public string LocationName { get; set; }
        public string AddressDescription { get; set; }
        public string CountryId { get; set; }
        public string RegionId { get; set; }
        public string CityId { get; set; }
        public string AreaId { get; set; }

        public string bankId { get; set; }
        public string AccountTitle { get; set; }
        public string AccountNo { get; set; }
        public string BranchCode { get; set; }
        public string ChequeImage { get; set; }
        public string LicenceFile { get; set; }
        public string CompanyRegNo { get; set; }
        public string CompanyRegDate { get; set; }
        public string NatureOfBusinessId { get; set; }
        public string NTNNumber { get; set; }
        public string BusinessAddress { get; set; }
        public string ReturnAddress { get; set; }
        public string TermsAndConditions { get; set; }
        public string NoOfProductYouListing { get; set; }
        public string AreYouSallingOnOtherWebsite { get; set; }
       public List<StoreBrands> storeBrands { get; set; }
    }
    public class StoreBrands
    {
        public string Id { get; set; }

        public string BrandId { get; set; }
        public string StoreId { get; set; }
    }
}
