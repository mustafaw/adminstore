﻿$(document).ready(function () {
    GetAllBrand();
})

var AddBrand = function () {
    var Id = $("#Id").val();
    var Brand = $("#Brand").val();
    var IsTop = $("input[name='IsTop']:checked").val();
    var SaveURL = $("#SaveURL").val();
    if (Brand != "") {
        $.ajax({
            url: SaveURL,
            type: "post",
            data: { Id: Id, Brand: Brand, IsTop: IsTop },
            success: function (res) {
                if (res == "true") {
                    swal({
                        title: "Saved", text: "Brand Saved Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else if (res == "updated") {
                    swal({
                        title: "Updated", text: "Brand Updated Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else {
                    swal({ title: "Error", text: res, type: "error" })
                }
            }
        })
    }
    else {
        $("#CategoryErrorField").html("Please Enter Brand Name")
    }

}
var GetAllBrand = function () {
    var GetallBrandsURL = $("#GetallBrandsURL").val();
    $.ajax({
        url: GetallBrandsURL,
        type: "post",
        data: {},
        success: function (res) {
            $("#DataHtml").html(res);
            $("#basic-1").DataTable();
        }
    })
}
var Edit = function (id, brand, IsTop) {
    debugger
    $("#Id").val(id);
    if (IsTop == "True") {
        $("input[name='IsTop']").prop("checked", true);
    }
    else {
        $("input[name='IsTop']").prop("checked", false);

    }
    $("#Brand").val(brand);
}
var Delete = function (id) {
    var URL = $("#DeleteBrandURL").val();
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it!'
    }).then(function () {
        $.ajax({
            url: URL,
            type: "POST",
            data: { Id: id },
            success: function (res) {
                if (res == "true") {
                    swal({
                        title: "Deleted", text: "Deleted Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else {
                    swal('Error!', res, 'error');
                }
            }
        })

    })
}