﻿$(document).ready(function () {
    getallCategory();
})

async function AddCategory() {
    var Id = $("#Id").val();
    var Category = $("#Category").val();
    var SaveURL = $("#SaveURL").val();
    var OrderBy = $("#OrderBy").val();
    var IsFront = $("input[name='IsFront']:checked").val();
    var ImageOne = await SAveImageOne()
    if (ImageOne == undefined || ImageOne == "") {
        ImageOne = $("#HideImageURL").val()
    }
    if (Category != "") {
        $.ajax({
            url: SaveURL,
            type: "post",
            data: {
                Id: Id, Category: Category, ImageOne: ImageOne, IsFront: IsFront
                ,OrderBy: OrderBy
            },
            success: function (res) {
                if (res == "true") {
                    swal({
                        title: "Saved", text: "Category Saved Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else if (res == "updated") {
                    swal({
                        title: "Updated", text: "Category Updated Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else {
                    swal({ title: "Error", text: res, type: "error" })
                }
            }
        })
    }
    else {
        $("#CategoryErrorField").html("Please Enter Category Name")
    }
   
}

///Save Profile Image
function SAveImageOne() {
    var ImageName;
    var input = document.getElementById("Image");
    var url = $("#SaveImageURL").val();
    var files = input.files;
    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i != files.length; i++) {
            formData.append("files", files[i]);
        }
        $.ajax(
            {
                url: url,
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                type: "POST",
                success: function (data) {

                    ImageName = data
                }
            }
        );
    }


    return ImageName;
}

var getallCategory = function () {
    var GetAllCategoryURL = $("#GetAllCategoryURL").val();
    $.ajax({
        url: GetAllCategoryURL,
        type: "post",
        data: {  },
        success: function (res) {
            $("#DataHtml").html(res);
            $("#basic-1").DataTable();
        }
    })
}
var Edit = function (id) {

    var URL = $("#EditCategoryURL").val();
    $.ajax({
        url: URL,
        type: "POST",
        data: { Id: id },
        success: function (res) {
            console.log(res);
            if (res.n.orderby != null) {
                $("#OrderBy").val(res.n.orderby)
            }
            else {
                $("#OrderBy").val("")
            }
            $("#Id").val(res.n.id);
            $("#Category").val(res.n.categoryName);
            $("#HideImageURL").val(res.n.image);
            if (res.n.isTop == true) {
                $("input[name='IsFront']").prop("checked", true);
            }
            else {
                $("input[name='IsFront']").prop("checked", false);

            }
            $(document).scrollTop(0);
        }
    })
}
var Delete = function (id) {
    var URL = $("#DeleteCategoryURL").val();
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it!'
    }).then(function () {
        $.ajax({
            url: URL,
            type: "POST",
            data: { Id: id },
            success: function (res) {
                if (res == "true") {
                    swal({
                        title: "Deleted", text: "Deleted Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else {
                    swal('Error!', res, 'error');
                }
            }
        })

    })
}