﻿$(document).ready(function () {

})

async function SaveDiscountBanner() {
    var Id = $("#Id").val();
    var Title = $("#Title").val();
    var Link = $("#Link").val();
    var ImageOne = await SAveImageOne()
    if (ImageOne == undefined || ImageOne == "") {
        ImageOne = $("#HideImgOneUrl").val()
    }

    var SaveURL = $("#SaveURL").val();

    $.ajax({
        url: SaveURL,
        async: true,
        type: "POST",
        data: {
            Id: Id, Title: Title, Link: Link, ImageOne: ImageOne

        },
        success: function (res) {
            if (res == "true") {
                swal("Success", "Addedd Succeffuly", "success").then(function () {
                    location.reload();
                })
            }
            else if (res == "updated") {
                swal("Success", "Updated Succeffuly", "success").then(function () {
                    location.reload();
                })
            }
            else {
                swal("Error", "Somethig went wrong", "error")
            }
        }
    })

}

var Edit = function (id) {
    var EditURL = $("#EditURL").val();

    $.ajax({
        url: EditURL,
        async: true,
        type: "POST",
        data: {
            Id: id
        },
        success: function (res) {
            console.log(res);
            $("#Id").val(res.s.id);
            $("#Title").val(res.s.title);
           
            $("#Link").val(res.s.link);
            $("#HideImgOneUrl").val(res.s.image)

            
            $(window).scrollTop(0);
        }
    })
}
var Delete = function (id) {
    var URL = $("#DeleteURL").val();
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it!'
    }).then(function () {
        $.ajax({
            url: URL,
            type: "POST",
            data: { Id: id },
            success: function (res) {
                if (res == "true") {
                    swal({
                        title: "Deleted", text: "Deleted Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }
                else {
                    swal('Error!', res, 'error');
                }
            }
        })

    })
}

///Save Profile Image
function SAveImageOne() {
    var ImageName;
    var input = document.getElementById("ImageOne");
    var url = $("#SaveImageOneURL").val();
    var files = input.files;
    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i != files.length; i++) {
            formData.append("files", files[i]);
        }
        $.ajax(
            {
                url: url,
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                type: "POST",
                success: function (data) {

                    ImageName = data
                }
            }
        );
    }


    return ImageName;
}

