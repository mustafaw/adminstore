﻿$(document).ready(function () {
    $("#DescriptionProduct").summernote({
        height: 200,
        placeholder: 'Add Product Description',
    });
    GetallProducts();
})
var getallStoreDRP = function () {
    var URL = $("#GetAllStoreDRPURL").val();
    var UserID = $("#UserID").val();

    $.ajax({
        url: URL,
        async: true,
        type: "POST",
        data: {
            UserID: UserID
        },
        success: function (res) {
            $("#StoreID").html(res);
            var Id = $("#HideStoreID").val();
            if (Id != "0") {
                $("#StoreID").val(Id).change();
            }

        }
    })
}
var Delete = function (id) {
    var URL = $("#DeleteURL").val();
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it!'
    }).then(function () {
        $.ajax({
            url: URL,
            type: "POST",
            data: { Id: id },
            success: function (res) {
                if (res == "true") {
                    swal({
                        title: "Deleted", text: "Deleted Succeffully !", type: "success"
                    }).then(function () {
                        location.reload();
                    });
                }

                else {
                    swal('Error!', res, 'error');
                }
            }
        })

    })
}
async function SaveProduct() {
    var Id = $("#Id").val();
    var UserID = $("#UserID").val();
    var StoreID = $("#StoreID").val();
    var BrandId = $("#BrandId").val();
    var CategoryId = $("#Category").val();
    var SecondCategory = $("#SecondCategory").val();
    var ThirdCategory = $("#ThirdCategory").val();
    var FourthCategory = $("#FourthCategory").val();
    var ProductName = $("#ProductName").val();
    var ProductNameInEnglish = $("#ProductNameInEnglish").val();
    var Highlights = $("#Highlights").val();
    var HighlightsInEnglish = $("#HighlightsInEnglish").val();
    var UnitID = $("#UnitID").val();

    var WarrantyType = $("#WarrantyType").val();
    var WhatsInBox = $("#WhatsInBox").val();
    var PackageWeightInKG = $("#PackageWeightInKG").val();
    var PackageLength = $("#PackageLength").val();
    var PackageWidth = $("#PackageWidth").val();
    var PackhageHeight = $("#PackhageHeight").val();
    var ColorFamily = $("#ColorFamily").val();
    var SKU = $("#SKU").val();
    var TotalQuantity = $("#TotalQuantity").val();
    var UnitPrice = $("#UnitPrice").val();
    var SpecialPrice = $("#SpecialPrice").val();

    var IsNew = $("input[name='IsNew']:checked").val();
    var IsFeatured = $("input[name='IsFeatured']:checked").val();
    var IsBest = $("input[name='IsBest']:checked").val();
    var IsSpecial = $("input[name='IsSpecial']:checked").val();
    var IsSale = $("input[name='IsSale']:checked").val();

    var IsFront = $("input[name='IsFront']:checked").val();
    var DescriptionProduct = $('#DescriptionProduct').summernote('code');
    var Thumbnail = await UploadThumbnails()
    if (Thumbnail == undefined || Thumbnail == "") {
        Thumbnail = $("#HideThumbnail").val()
    }

    var SaveURL = $("#SaveURL").val();
    if (CommonJs.Validate_Form('StoreForm')) {
        $.ajax({
            url: SaveURL,
            async: true,
            type: "POST",
            data: {
                Id: Id, UserID: UserID, StoreID: StoreID, BrandId: BrandId, CategoryId: CategoryId,
                SecondCategory: SecondCategory, ThirdCategory: ThirdCategory, FourthCategory: FourthCategory,
                ProductName: ProductName, ProductNameInEnglish: ProductNameInEnglish,
                Highlights: Highlights, HighlightsInEnglish: HighlightsInEnglish,
                WarrantyType: WarrantyType, WhatsInBox: WhatsInBox, UnitID, UnitID,
                PackageWeightInKG: PackageWeightInKG, PackageLength: PackageLength, PackageWidth: PackageWidth,
                PackhageHeight: PackhageHeight, ColorFamily: ColorFamily
                , SKU: SKU, TotalQuantity: TotalQuantity, UnitPrice: UnitPrice,
                SpecialPrice: SpecialPrice, IsNew: IsNew, IsFeatured: IsFeatured, IsBest: IsBest, IsSpecial: IsSpecial,
                IsSale: IsSale, Thumbnail: Thumbnail, DescriptionProduct: DescriptionProduct, IsFront: IsFront
            },
            success: function (res) {
                if (res == "true") {
                    swal("Success", "Store Addedd Succeffuly", "success").then(function () {
                        location.reload();
                    })
                }
                else if (res == "updated") {
                    swal("Success", "Store Updated Succeffuly", "success").then(function () {
                        location.reload();
                    })
                }
                else {
                    swal("Error", res, "error")
                }
            }
        })
    }


}
var Edit = function (id) {
    var URL = $("#EditProductURL").val();

    $.ajax({
        url: URL,
        async: true,
        type: "POST",
        data: {
            Id: id
        },
        success: function (res) {
            console.log(res);
            $("#Id").val(res.n.id);
            $("#UserID").val(res.n.userId).change();
            $("#HideStoreID").val(res.n.storeID);
            $("#BrandId").val(res.n.brandId);
            $("#HideSecondCategoryId").val(res.n.secondCategory);
            $("#HideThirdCategoryId").val(res.n.thirdCategory);
            $("#HideFourthCategoryId").val(res.n.fourthCategory);
            $("#Category").val(res.n.categoryId).change();
            $("#ProductName").val(res.n.productName);
            $("#ProductNameInEnglish").val(res.n.productNameInEnglish);
            $("#Highlights").val(res.n.highlights);
            $("#UnitID").val(res.n.uomId)
            $("#HighlightsInEnglish").val(res.n.englishHighlights);
            $("#WarrantyType").val(res.n.warrantyType).change();
            $("#WhatsInBox").val(res.n.whatInBox);
            $("#PackageWeightInKG").val(res.n.packageWeghtInKg);
            $("#PackageLength").val(res.n.length);
            $("#PackageWidth").val(res.n.width);
            $("#PackhageHeight").val(res.n.height);
            $("#ColorFamily").val(res.n.colorFamily);
            $("#SKU").val(res.n.sellerSKU);
            $("#TotalQuantity").val(res.n.availableQuantity);
            $("#UnitPrice").val(res.n.price);
            $("#UnitPrice").prop("disabled", "true");
            $("#SpecialPrice").val(res.n.specialPrice);
            if (res.n.isNew == "True") {
                $("input[name='IsNew']").prop("checked", true);
            }
            else {
                $("input[name='IsNew']").prop("checked", false);
            }
            if (res.n.isSale == "True") {
                $("input[name='IsSale']").prop("checked", true);
            }
            else {
                $("input[name='IsSale']").prop("checked", false);
            }
            if (res.n.isFront == "True") {
                $("input[name='IsFront']").prop("checked", true);
            }
            else {
                $("input[name='IsFront']").prop("checked", false);
            }
            if (res.n.isSpecial == "True") {
                $("input[name='IsSpecial']").prop("checked", true);
            }
            else {
                $("input[name='IsSpecial']").prop("checked", false);
            }
            if (res.n.isBest == "True") {
                $("input[name='IsBest']").prop("checked", true);
            }
            else {
                $("input[name='IsBest']").prop("checked", false);
            }
            if (res.n.isFeatured == "True") {
                $("input[name='IsFeatured']").prop("checked", true);
            }
            else {
                $("input[name='IsFeatured']").prop("checked", false);
            }
            $('#DescriptionProduct').summernote('code', res.n.productDescription);
            $("#HideThumbnail").val(res.n.iThumbNailImaged);

            $(window).scrollTop(0);

        }
    })
}
var GetallProducts = function (id) {
    var URL = $("#GetallProductsURL").val();
    $.ajax({
        url: URL,
        async: true,
        type: "POST",
        data: {},
        success: function (res) {
            $("#DataHtml").html(res);
            $("#basic-1").DataTable();
        }
    })
}

///Save Cheque Image
function UploadThumbnails() {
    var ImageName;
    var input = document.getElementById("productThumbnails");
    var url = $("#SaveProductThumbnailsURL").val();
    var files = input.files;
    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i != files.length; i++) {
            formData.append("files", files[i]);
        }
        $.ajax(
            {
                url: url,
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                type: "POST",
                success: function (data) {

                    ImageName = data
                }
            }
        );
    }


    return ImageName;
}