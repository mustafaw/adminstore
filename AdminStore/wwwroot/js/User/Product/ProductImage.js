﻿var getallStoreDRP = function () {
    var URL = $("#GetAllStoreDRPURL").val();
    var UserID = $("#UserID").val();

    $.ajax({
        url: URL,
        async: true,
        type: "POST",
        data: {
            UserID: UserID
        },
        success: function (res) {
            $("#StoreID").html(res);
           
        }
    })
}
var GetallStoreProductDRP = function () {
    var URL = $("#GetallStoreProductURL").val();
    var StoreID = $("#StoreID").val();

    $.ajax({
        url: URL,
        async: true,
        type: "POST",
        data: {
            StoreID: StoreID
        },
        success: function (res) {
            $("#Product").html(res);
          
        }
    })
}


$(document).ready(function () {
    GetallProducts();
});



var myDropzone = new Dropzone(
    "#my-dropzone",
    {

        uploadMultiple: true,
        autoProcessQueue: false,
        maxFilesize: 30,
        acceptedFiles: "image/*",
        timeout: 180000,
        parallelUploads: 10
    }
);


function SendFilesToServer() {
    var Product = $("#Product").val();

    if (Product == undefined || Product == "--Select Product--"||Product=="") {
        Product = "";
    }
    if (Product !="") {
        myDropzone.processQueue();
    }
    else {
        swal("Error", "Please Select Product First", "error");
    }

}

myDropzone.on("sending", function (file, xhr, formData) {
  
    var Product = $("#Product").val();

    formData.append("Product", Product );




});
myDropzone.on("complete", function (file) {
    myDropzone.removeFile(file);
});

myDropzone.on("success", function (file, res) {
    if (file.status == "success") {
        swal({
            title: "Success", text: "Images Addedd Successfully!", type: "success"
        }).then(function () {
            location.reload();
        });
    }

});
var GetallProducts = function (id) {
    var URL = $("#GetallProductImagesURL").val();
    $.ajax({
        url: URL,
        async: true,
        type: "POST",
        data: {},
        success: function (res) {
            $("#DataHtml").html(res);
            $("#basic-1").DataTable();
        }
    })
}
var ProductId = "";
var ViewImages = function (id) {
    ProductId = id;
    var URL = $("#ViewProductImagesURL").val();
    $.ajax({
        url: URL,
        async: true,
        type: "POST",
        data: { id: id},
        success: function (res) {
            $("#ImagesModal").modal();
            $("#ModalBody").html(res);
        }
    })
}

var RemoveImage = function (id) {
   
    var url = $("#HidRemoveImageURL").val();
    $.ajax({
        url: url,
        async: true,
        type: "POST",
        data: {
            id: id
        },
        success: function (res) {
            if (res == "true") {
                ViewImages(ProductId);
            }
            else if (res == "file Not Found") {
                swal("Error", "File Not Found", "error");
            }
            else {
                swal("Error", res, "error");
            }

        }
    })
}