﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblBrand
    {
        public TblBrand()
        {
            TblProduct = new HashSet<TblProduct>();
            TblStoreBrand = new HashSet<TblStoreBrand>();
        }

        public decimal Id { get; set; }
        public string BrandName { get; set; }
        public string BannerImage { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool? IsTop { get; set; }

        public virtual ICollection<TblProduct> TblProduct { get; set; }
        public virtual ICollection<TblStoreBrand> TblStoreBrand { get; set; }
    }
}
