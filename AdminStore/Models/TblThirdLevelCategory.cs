﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblThirdLevelCategory
    {
        public TblThirdLevelCategory()
        {
            TblForthLevelCategory = new HashSet<TblForthLevelCategory>();
            TblProduct = new HashSet<TblProduct>();
        }

        public decimal Id { get; set; }
        public string CategoryName { get; set; }
        public decimal? SecondLevelCategoryId { get; set; }

        public virtual TblSecondLevelCategory SecondLevelCategory { get; set; }
        public virtual ICollection<TblForthLevelCategory> TblForthLevelCategory { get; set; }
        public virtual ICollection<TblProduct> TblProduct { get; set; }
    }
}
