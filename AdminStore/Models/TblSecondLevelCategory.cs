﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblSecondLevelCategory
    {
        public TblSecondLevelCategory()
        {
            TblProduct = new HashSet<TblProduct>();
            TblThirdLevelCategory = new HashSet<TblThirdLevelCategory>();
        }

        public decimal Id { get; set; }
        public string CategoryName { get; set; }
        public decimal? CategoryId { get; set; }

        public virtual TblCategory Category { get; set; }
        public virtual ICollection<TblProduct> TblProduct { get; set; }
        public virtual ICollection<TblThirdLevelCategory> TblThirdLevelCategory { get; set; }
    }
}
