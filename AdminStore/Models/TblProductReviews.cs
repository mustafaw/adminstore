﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblProductReviews
    {
        public decimal Id { get; set; }
        public decimal? ProductId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Review { get; set; }
        public decimal? Rating { get; set; }
        public DateTime? Date { get; set; }
        public string Subject { get; set; }

        public virtual TblProduct Product { get; set; }
    }
}
