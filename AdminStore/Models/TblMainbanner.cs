﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblMainbanner
    {
        public decimal Id { get; set; }
        public string FirstImage { get; set; }
        public string SecondImage { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
    }
}
