﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblSubbanners
    {
        public decimal Id { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Link { get; set; }
        public bool? IsTop { get; set; }
        public bool? IsBottom { get; set; }
    }
}
