﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblOrderPackages
    {
        public decimal Id { get; set; }
        public decimal? OrderId { get; set; }
        public decimal? StoreId { get; set; }
        public decimal? ProductId { get; set; }
        public string ProductName { get; set; }
        public int? Quantity { get; set; }
        public decimal? Price { get; set; }
        public decimal? Total { get; set; }
        public int? PackageNo { get; set; }
        public string PackageStatus { get; set; }
    }
}
