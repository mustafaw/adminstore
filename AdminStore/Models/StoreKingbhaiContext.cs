﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AdminStore.Models
{
    public partial class StoreKingbhaiContext : DbContext
    {
        public StoreKingbhaiContext()
        {
        }

        public StoreKingbhaiContext(DbContextOptions<StoreKingbhaiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<TblAddress> TblAddress { get; set; }
        public virtual DbSet<TblArea> TblArea { get; set; }
        public virtual DbSet<TblBankDetail> TblBankDetail { get; set; }
        public virtual DbSet<TblBrand> TblBrand { get; set; }
        public virtual DbSet<TblCategory> TblCategory { get; set; }
        public virtual DbSet<TblCity> TblCity { get; set; }
        public virtual DbSet<TblCountry> TblCountry { get; set; }
        public virtual DbSet<TblDiscountbanner> TblDiscountbanner { get; set; }
        public virtual DbSet<TblForthLevelCategory> TblForthLevelCategory { get; set; }
        public virtual DbSet<TblGetQuote> TblGetQuote { get; set; }
        public virtual DbSet<TblMainbanner> TblMainbanner { get; set; }
        public virtual DbSet<TblNatureOfBusiness> TblNatureOfBusiness { get; set; }
        public virtual DbSet<TblOrder> TblOrder { get; set; }
        public virtual DbSet<TblOrderPackages> TblOrderPackages { get; set; }
        public virtual DbSet<TblProduct> TblProduct { get; set; }
        public virtual DbSet<TblProductImages> TblProductImages { get; set; }
        public virtual DbSet<TblProductReviews> TblProductReviews { get; set; }
        public virtual DbSet<TblProductUnit> TblProductUnit { get; set; }
        public virtual DbSet<TblRegion> TblRegion { get; set; }
        public virtual DbSet<TblSecondLevelCategory> TblSecondLevelCategory { get; set; }
        public virtual DbSet<TblStore> TblStore { get; set; }
        public virtual DbSet<TblStoreBrand> TblStoreBrand { get; set; }
        public virtual DbSet<TblSubbanners> TblSubbanners { get; set; }
        public virtual DbSet<TblThirdLevelCategory> TblThirdLevelCategory { get; set; }
        public virtual DbSet<TblUserDetail> TblUserDetail { get; set; }
        public virtual DbSet<TblWebReviews> TblWebReviews { get; set; }
        public virtual DbSet<TblWishlist> TblWishlist { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=.\\MSSQLSERVER2012;Database=WinStoredb;User ID=KingbhaiWinStore;password=9Fujo81&;");

                //optionsBuilder.UseSqlServer("Data Source=KB-EMP26\\SQLEXPRESS2012;Initial Catalog=StoreKingbhai;Integrated Security=True;Persist Security Info=False");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedName] IS NOT NULL)");

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.ProviderKey).HasMaxLength(128);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasIndex(e => e.RoleId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.Name).HasMaxLength(128);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.NormalizedEmail)
                    .HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName)
                    .HasName("UserNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedUserName] IS NOT NULL)");

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<TblAddress>(entity =>
            {
                entity.ToTable("Tbl_Address");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AddressDescription).IsUnicode(false);

                entity.Property(e => e.AreaId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CityId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CountryId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Latitude).HasColumnName("latitude");

                entity.Property(e => e.LocationName).IsUnicode(false);

                entity.Property(e => e.Longitude).HasColumnName("longitude");

                entity.Property(e => e.RegionId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.StoreId).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.Area)
                    .WithMany(p => p.TblAddress)
                    .HasForeignKey(d => d.AreaId)
                    .HasConstraintName("FK_Tbl_Address_Tbl_Area");

                entity.HasOne(d => d.City)
                    .WithMany(p => p.TblAddress)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("FK_Tbl_Address_Tbl_City");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.TblAddress)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_Tbl_Address_Tbl_Country");

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.TblAddress)
                    .HasForeignKey(d => d.RegionId)
                    .HasConstraintName("FK_Tbl_Address_Tbl_Region");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.TblAddress)
                    .HasForeignKey(d => d.StoreId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Tbl_Address_Tbl_Store");
            });

            modelBuilder.Entity<TblArea>(entity =>
            {
                entity.ToTable("Tbl_Area");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AreaName).IsUnicode(false);

                entity.Property(e => e.CityId).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.City)
                    .WithMany(p => p.TblArea)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("FK_Tbl_Area_Tbl_City");
            });

            modelBuilder.Entity<TblBankDetail>(entity =>
            {
                entity.ToTable("Tbl_BankDetail");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AccountNo).HasMaxLength(100);

                entity.Property(e => e.BranchCode).HasMaxLength(100);

                entity.Property(e => e.StoreId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UserId).HasMaxLength(450);

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.TblBankDetail)
                    .HasForeignKey(d => d.StoreId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Tbl_BankDetail_Tbl_Store");
            });

            modelBuilder.Entity<TblBrand>(entity =>
            {
                entity.ToTable("Tbl_Brand");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BrandName).HasMaxLength(500);

                entity.Property(e => e.Title).HasMaxLength(500);
            });

            modelBuilder.Entity<TblCategory>(entity =>
            {
                entity.ToTable("Tbl_Category");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CategoryName).HasMaxLength(500);
            });

            modelBuilder.Entity<TblCity>(entity =>
            {
                entity.ToTable("Tbl_City");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CityName).IsUnicode(false);

                entity.Property(e => e.RegionId).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.TblCity)
                    .HasForeignKey(d => d.RegionId)
                    .HasConstraintName("FK_Tbl_City_Tbl_Region");
            });

            modelBuilder.Entity<TblCountry>(entity =>
            {
                entity.ToTable("Tbl_Country");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CountryName).IsUnicode(false);
            });

            modelBuilder.Entity<TblDiscountbanner>(entity =>
            {
                entity.ToTable("Tbl_Discountbanner");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<TblForthLevelCategory>(entity =>
            {
                entity.ToTable("Tbl_ForthLevelCategory");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CategoryName).HasMaxLength(250);

                entity.Property(e => e.TblThirdLevelCategoryId)
                    .HasColumnName("Tbl_ThirdLevelCategoryId")
                    .HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.TblThirdLevelCategory)
                    .WithMany(p => p.TblForthLevelCategory)
                    .HasForeignKey(d => d.TblThirdLevelCategoryId)
                    .HasConstraintName("FK_Tbl_ForthLevelCategory_Tbl_ThirdLevelCategory");
            });

            modelBuilder.Entity<TblGetQuote>(entity =>
            {
                entity.ToTable("Tbl_GetQuote");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.ProductId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.StoreId).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<TblMainbanner>(entity =>
            {
                entity.ToTable("Tbl_Mainbanner");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.SubTitle).HasMaxLength(500);

                entity.Property(e => e.Title).HasMaxLength(500);
            });

            modelBuilder.Entity<TblNatureOfBusiness>(entity =>
            {
                entity.ToTable("Tbl_NatureOfBusiness");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.NatureOfBusiness).HasMaxLength(500);
            });

            modelBuilder.Entity<TblOrder>(entity =>
            {
                entity.ToTable("Tbl_Order");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.OrderNo).HasMaxLength(250);

                entity.Property(e => e.PaymentStatus)
                    .HasMaxLength(250)
                    .HasDefaultValueSql("(N'Pending')");

                entity.Property(e => e.PaymentType).HasMaxLength(250);

                entity.Property(e => e.PlacedOn).HasColumnType("datetime");

                entity.Property(e => e.TotalAmount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UserId).HasMaxLength(450);
            });

            modelBuilder.Entity<TblOrderPackages>(entity =>
            {
                entity.ToTable("Tbl_OrderPackages");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.OrderId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PackageStatus)
                    .HasMaxLength(250)
                    .HasDefaultValueSql("(N'Placed')");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ProductId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.StoreId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Total).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<TblProduct>(entity =>
            {
                entity.ToTable("Tbl_Product");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AvailableQuantity).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BrandId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CategoryId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ColorFamily).HasMaxLength(500);

                entity.Property(e => e.CreatedOn).HasColumnType("date");

                entity.Property(e => e.DangerousGoods).HasMaxLength(500);

                entity.Property(e => e.ForthLevelCategoryId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Height).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.IsBest).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsFeatured).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsNew).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsSale).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsSpecial).HasDefaultValueSql("((0))");

                entity.Property(e => e.Length).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PackageWeghtInKg).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ProductUnitId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SecondLevelCategoryId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SellerSku).HasColumnName("SellerSKU");

                entity.Property(e => e.SpecialPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Status)
                    .HasMaxLength(20)
                    .HasDefaultValueSql("(N'Enable')");

                entity.Property(e => e.StoreId)
                    .HasColumnName("StoreID")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ThirdLevelCategoryId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TotalQuantity).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UserId).HasMaxLength(450);

                entity.Property(e => e.WarrantyType).HasMaxLength(500);

                entity.Property(e => e.Width).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.Brand)
                    .WithMany(p => p.TblProduct)
                    .HasForeignKey(d => d.BrandId)
                    .HasConstraintName("FK_Tbl_Product_Tbl_Brand");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.TblProduct)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_Tbl_Product_Tbl_Category");

                entity.HasOne(d => d.ProductUnit)
                    .WithMany(p => p.TblProduct)
                    .HasForeignKey(d => d.ProductUnitId)
                    .HasConstraintName("FK_Tbl_Product_Tbl_ProductUnit");

                entity.HasOne(d => d.SecondLevelCategory)
                    .WithMany(p => p.TblProduct)
                    .HasForeignKey(d => d.SecondLevelCategoryId)
                    .HasConstraintName("FK_Tbl_Product_Tbl_SecondLevelCategory");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.TblProduct)
                    .HasForeignKey(d => d.StoreId)
                    .HasConstraintName("FK_Tbl_Product_Tbl_Store");

                entity.HasOne(d => d.ThirdLevelCategory)
                    .WithMany(p => p.TblProduct)
                    .HasForeignKey(d => d.ThirdLevelCategoryId)
                    .HasConstraintName("FK_Tbl_Product_Tbl_ForthLevelCategory");

                entity.HasOne(d => d.ThirdLevelCategoryNavigation)
                    .WithMany(p => p.TblProduct)
                    .HasForeignKey(d => d.ThirdLevelCategoryId)
                    .HasConstraintName("FK_Tbl_Product_Tbl_ThirdLevelCategory");
            });

            modelBuilder.Entity<TblProductImages>(entity =>
            {
                entity.ToTable("Tbl_ProductImages");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ProductId).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TblProductImages)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_Tbl_ProductImages_Tbl_Product");
            });

            modelBuilder.Entity<TblProductReviews>(entity =>
            {
                entity.ToTable("Tbl_ProductReviews");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(250);

                entity.Property(e => e.ProductId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Rating).HasColumnType("decimal(18, 1)");

                entity.Property(e => e.UserName).HasMaxLength(250);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TblProductReviews)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_Tbl_ProductReviews_Tbl_Product");
            });

            modelBuilder.Entity<TblProductUnit>(entity =>
            {
                entity.ToTable("Tbl_ProductUnit");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.UnitName).HasMaxLength(250);
            });

            modelBuilder.Entity<TblRegion>(entity =>
            {
                entity.ToTable("Tbl_Region");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CountryId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.RegionName).IsUnicode(false);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.TblRegion)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_Tbl_Region_Tbl_Country");
            });

            modelBuilder.Entity<TblSecondLevelCategory>(entity =>
            {
                entity.ToTable("Tbl_SecondLevelCategory");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CategoryId).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.TblSecondLevelCategory)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_Tbl_SecondLevelCategory_Tbl_Category");
            });

            modelBuilder.Entity<TblStore>(entity =>
            {
                entity.ToTable("Tbl_Store");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AddressId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BankId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CategoryId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyLicence).HasMaxLength(500);

                entity.Property(e => e.CompanyRegDate).HasColumnType("date");

                entity.Property(e => e.CompanyRegNo).HasMaxLength(500);

                entity.Property(e => e.CreatedOn).HasColumnType("date");

                entity.Property(e => e.NatureOfBusinessId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.NoOfProductYouListing).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Ntnnumber).HasColumnName("NTNNumber");

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(450);

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.TblStore)
                    .HasForeignKey(d => d.AddressId)
                    .HasConstraintName("FK_Tbl_Store_Tbl_Address");

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.TblStore)
                    .HasForeignKey(d => d.BankId)
                    .HasConstraintName("FK_Tbl_Store_Tbl_BankDetail");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.TblStore)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_Tbl_Store_Tbl_Category");

                entity.HasOne(d => d.NatureOfBusiness)
                    .WithMany(p => p.TblStore)
                    .HasForeignKey(d => d.NatureOfBusinessId)
                    .HasConstraintName("FK_Tbl_Store_Tbl_NatureOfBusiness");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblStore)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Tbl_Store_AspNetUsers");
            });

            modelBuilder.Entity<TblStoreBrand>(entity =>
            {
                entity.ToTable("Tbl_StoreBrand");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BrandId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.StoreId).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.Brand)
                    .WithMany(p => p.TblStoreBrand)
                    .HasForeignKey(d => d.BrandId)
                    .HasConstraintName("FK_Tbl_StoreBrand_Tbl_Brand");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.TblStoreBrand)
                    .HasForeignKey(d => d.StoreId)
                    .HasConstraintName("FK_Tbl_StoreBrand_Tbl_Store");
            });

            modelBuilder.Entity<TblSubbanners>(entity =>
            {
                entity.ToTable("Tbl_Subbanners");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IsBottom)
                    .HasColumnName("Is_Bottom")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsTop)
                    .HasColumnName("Is_Top")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SubTitle).HasMaxLength(200);

                entity.Property(e => e.Title).HasMaxLength(200);
            });

            modelBuilder.Entity<TblThirdLevelCategory>(entity =>
            {
                entity.ToTable("Tbl_ThirdLevelCategory");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.SecondLevelCategoryId).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.SecondLevelCategory)
                    .WithMany(p => p.TblThirdLevelCategory)
                    .HasForeignKey(d => d.SecondLevelCategoryId)
                    .HasConstraintName("FK_Tbl_ThirdLevelCategory_Tbl_SecondLevelCategory");
            });

            modelBuilder.Entity<TblUserDetail>(entity =>
            {
                entity.ToTable("Tbl_UserDetail");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AddressId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BankId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Cnic).HasColumnName("CNIC");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dob).HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(250);

                entity.Property(e => e.Ip).HasColumnName("IP");

                entity.Property(e => e.Mobile).HasMaxLength(250);

                entity.Property(e => e.ProfilePicture).HasMaxLength(250);

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(450);

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.TblUserDetail)
                    .HasForeignKey(d => d.AddressId)
                    .HasConstraintName("FK_Tbl_UserDetail_Tbl_Address");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblUserDetail)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Tbl_UserDetail_AspNetUsers");
            });

            modelBuilder.Entity<TblWebReviews>(entity =>
            {
                entity.ToTable("Tbl_WebReviews");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IsFront)
                    .HasColumnName("Is_Front")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UserId).HasMaxLength(450);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblWebReviews)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Tbl_WebReviews_AspNetUsers");
            });

            modelBuilder.Entity<TblWishlist>(entity =>
            {
                entity.ToTable("Tbl_Wishlist");

                entity.Property(e => e.Id)
                    .HasColumnType("decimal(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ProductId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UserId).HasMaxLength(450);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
