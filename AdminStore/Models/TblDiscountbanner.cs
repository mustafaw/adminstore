﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblDiscountbanner
    {
        public decimal Id { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
    }
}
