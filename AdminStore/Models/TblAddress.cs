﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblAddress
    {
        public TblAddress()
        {
            TblStore = new HashSet<TblStore>();
            TblUserDetail = new HashSet<TblUserDetail>();
        }

        public decimal Id { get; set; }
        public string LocationName { get; set; }
        public string AddressDescription { get; set; }
        public decimal? CountryId { get; set; }
        public decimal? RegionId { get; set; }
        public decimal? CityId { get; set; }
        public decimal? AreaId { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public decimal? StoreId { get; set; }

        public virtual TblArea Area { get; set; }
        public virtual TblCity City { get; set; }
        public virtual TblCountry Country { get; set; }
        public virtual TblRegion Region { get; set; }
        public virtual TblStore Store { get; set; }
        public virtual ICollection<TblStore> TblStore { get; set; }
        public virtual ICollection<TblUserDetail> TblUserDetail { get; set; }
    }
}
