﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblOrder
    {
        public decimal Id { get; set; }
        public string OrderNo { get; set; }
        public decimal? TotalAmount { get; set; }
        public string PaymentType { get; set; }
        public string PaymentStatus { get; set; }
        public string UserId { get; set; }
        public DateTime? PlacedOn { get; set; }
    }
}
