﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblArea
    {
        public TblArea()
        {
            TblAddress = new HashSet<TblAddress>();
        }

        public decimal Id { get; set; }
        public string AreaName { get; set; }
        public decimal? CityId { get; set; }

        public virtual TblCity City { get; set; }
        public virtual ICollection<TblAddress> TblAddress { get; set; }
    }
}
