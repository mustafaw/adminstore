﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblProduct
    {
        public TblProduct()
        {
            TblProductImages = new HashSet<TblProductImages>();
            TblProductReviews = new HashSet<TblProductReviews>();
        }

        public decimal Id { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string UserId { get; set; }
        public string ProductName { get; set; }
        public decimal? ProductUnitId { get; set; }
        public string ProductNameInEnglish { get; set; }
        public decimal? BrandId { get; set; }
        public decimal? CategoryId { get; set; }
        public decimal? SecondLevelCategoryId { get; set; }
        public decimal? ThirdLevelCategoryId { get; set; }
        public decimal? ForthLevelCategoryId { get; set; }
        public decimal? StoreId { get; set; }
        public string Highlights { get; set; }
        public string ProductDescription { get; set; }
        public string EnglishHighlights { get; set; }
        public string WarrantyType { get; set; }
        public string DangerousGoods { get; set; }
        public string WhatInBox { get; set; }
        public decimal? PackageWeghtInKg { get; set; }
        public decimal? Length { get; set; }
        public decimal? Width { get; set; }
        public decimal? Height { get; set; }
        public string ColorFamily { get; set; }
        public string SellerSku { get; set; }
        public decimal? TotalQuantity { get; set; }
        public decimal? AvailableQuantity { get; set; }
        public decimal? Price { get; set; }
        public decimal? SpecialPrice { get; set; }
        public string ThumbNailImage { get; set; }
        public bool? IsNew { get; set; }
        public bool? IsFeatured { get; set; }
        public bool? IsBest { get; set; }
        public bool? IsSale { get; set; }
        public bool? IsSpecial { get; set; }
        public string Status { get; set; }
        public bool? IsFront { get; set; }

        public virtual TblBrand Brand { get; set; }
        public virtual TblCategory Category { get; set; }
        public virtual TblProductUnit ProductUnit { get; set; }
        public virtual TblSecondLevelCategory SecondLevelCategory { get; set; }
        public virtual TblStore Store { get; set; }
        public virtual TblForthLevelCategory ThirdLevelCategory { get; set; }
        public virtual TblThirdLevelCategory ThirdLevelCategoryNavigation { get; set; }
        public virtual ICollection<TblProductImages> TblProductImages { get; set; }
        public virtual ICollection<TblProductReviews> TblProductReviews { get; set; }
    }
}
