﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblStore
    {
        public TblStore()
        {
            TblAddress = new HashSet<TblAddress>();
            TblBankDetail = new HashSet<TblBankDetail>();
            TblProduct = new HashSet<TblProduct>();
            TblStoreBrand = new HashSet<TblStoreBrand>();
        }

        public decimal Id { get; set; }
        public string UserId { get; set; }
        public string CompanyName { get; set; }
        public string Description { get; set; }
        public string CompanyLogo { get; set; }
        public string CompanyLicence { get; set; }
        public decimal? AddressId { get; set; }
        public decimal? CategoryId { get; set; }
        public decimal? BankId { get; set; }
        public string LicenceFile { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CompanyRegNo { get; set; }
        public DateTime? CompanyRegDate { get; set; }
        public decimal? NatureOfBusinessId { get; set; }
        public string Ntnnumber { get; set; }
        public string BusinessAddress { get; set; }
        public string ReturnAddress { get; set; }
        public bool? TermsAndConditions { get; set; }
        public decimal? NoOfProductYouListing { get; set; }
        public bool? AreYouSallingOnOtherWebsite { get; set; }

        public virtual TblAddress Address { get; set; }
        public virtual TblBankDetail Bank { get; set; }
        public virtual TblCategory Category { get; set; }
        public virtual TblNatureOfBusiness NatureOfBusiness { get; set; }
        public virtual AspNetUsers User { get; set; }
        public virtual ICollection<TblAddress> TblAddress { get; set; }
        public virtual ICollection<TblBankDetail> TblBankDetail { get; set; }
        public virtual ICollection<TblProduct> TblProduct { get; set; }
        public virtual ICollection<TblStoreBrand> TblStoreBrand { get; set; }
    }
}
