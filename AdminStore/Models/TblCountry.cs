﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblCountry
    {
        public TblCountry()
        {
            TblAddress = new HashSet<TblAddress>();
            TblRegion = new HashSet<TblRegion>();
        }

        public decimal Id { get; set; }
        public string CountryName { get; set; }

        public virtual ICollection<TblAddress> TblAddress { get; set; }
        public virtual ICollection<TblRegion> TblRegion { get; set; }
    }
}
