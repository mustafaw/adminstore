﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblWebReviews
    {
        public decimal Id { get; set; }
        public string UserId { get; set; }
        public string Description { get; set; }
        public bool? IsFront { get; set; }

        public virtual AspNetUsers User { get; set; }
    }
}
