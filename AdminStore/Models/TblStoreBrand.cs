﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblStoreBrand
    {
        public decimal Id { get; set; }
        public decimal? BrandId { get; set; }
        public decimal? StoreId { get; set; }

        public virtual TblBrand Brand { get; set; }
        public virtual TblStore Store { get; set; }
    }
}
