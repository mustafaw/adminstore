﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblForthLevelCategory
    {
        public TblForthLevelCategory()
        {
            TblProduct = new HashSet<TblProduct>();
        }

        public decimal Id { get; set; }
        public string CategoryName { get; set; }
        public decimal? TblThirdLevelCategoryId { get; set; }

        public virtual TblThirdLevelCategory TblThirdLevelCategory { get; set; }
        public virtual ICollection<TblProduct> TblProduct { get; set; }
    }
}
