﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblNatureOfBusiness
    {
        public TblNatureOfBusiness()
        {
            TblStore = new HashSet<TblStore>();
        }

        public decimal Id { get; set; }
        public string NatureOfBusiness { get; set; }

        public virtual ICollection<TblStore> TblStore { get; set; }
    }
}
