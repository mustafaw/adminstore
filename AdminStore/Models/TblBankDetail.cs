﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblBankDetail
    {
        public TblBankDetail()
        {
            TblStore = new HashSet<TblStore>();
        }

        public decimal Id { get; set; }
        public string UserId { get; set; }
        public string AccountTitle { get; set; }
        public string AccountNo { get; set; }
        public string BranchCode { get; set; }
        public string ChequeImage { get; set; }
        public decimal? StoreId { get; set; }

        public virtual TblStore Store { get; set; }
        public virtual ICollection<TblStore> TblStore { get; set; }
    }
}
