﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblProductImages
    {
        public decimal Id { get; set; }
        public string ImagePath { get; set; }
        public decimal? ProductId { get; set; }

        public virtual TblProduct Product { get; set; }
    }
}
