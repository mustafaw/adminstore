﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblGetQuote
    {
        public decimal Id { get; set; }
        public decimal? StoreId { get; set; }
        public decimal? ProductId { get; set; }
        public string CoustomerName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string DeliveryLocation { get; set; }
        public string Requirements { get; set; }
        public DateTime? Date { get; set; }
    }
}
