﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblRegion
    {
        public TblRegion()
        {
            TblAddress = new HashSet<TblAddress>();
            TblCity = new HashSet<TblCity>();
        }

        public decimal Id { get; set; }
        public string RegionName { get; set; }
        public decimal? CountryId { get; set; }

        public virtual TblCountry Country { get; set; }
        public virtual ICollection<TblAddress> TblAddress { get; set; }
        public virtual ICollection<TblCity> TblCity { get; set; }
    }
}
