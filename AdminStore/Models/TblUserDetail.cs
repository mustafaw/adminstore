﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblUserDetail
    {
        public decimal Id { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string Ip { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public DateTime? Dob { get; set; }
        public string Mobile { get; set; }
        public string ProfilePicture { get; set; }
        public string About { get; set; }
        public decimal? BankId { get; set; }
        public string Cnic { get; set; }
        public decimal? AddressId { get; set; }

        public virtual TblAddress Address { get; set; }
        public virtual AspNetUsers User { get; set; }
    }
}
