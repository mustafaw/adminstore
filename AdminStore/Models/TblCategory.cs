﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblCategory
    {
        public TblCategory()
        {
            TblProduct = new HashSet<TblProduct>();
            TblSecondLevelCategory = new HashSet<TblSecondLevelCategory>();
            TblStore = new HashSet<TblStore>();
        }

        public decimal Id { get; set; }
        public string CategoryName { get; set; }
        public string Image { get; set; }
        public bool? IsTop { get; set; }
        public int? Orderby { get; set; }

        public virtual ICollection<TblProduct> TblProduct { get; set; }
        public virtual ICollection<TblSecondLevelCategory> TblSecondLevelCategory { get; set; }
        public virtual ICollection<TblStore> TblStore { get; set; }
    }
}
