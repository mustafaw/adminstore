﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblCity
    {
        public TblCity()
        {
            TblAddress = new HashSet<TblAddress>();
            TblArea = new HashSet<TblArea>();
        }

        public decimal Id { get; set; }
        public string CityName { get; set; }
        public decimal? RegionId { get; set; }

        public virtual TblRegion Region { get; set; }
        public virtual ICollection<TblAddress> TblAddress { get; set; }
        public virtual ICollection<TblArea> TblArea { get; set; }
    }
}
