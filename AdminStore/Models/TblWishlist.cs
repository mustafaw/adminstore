﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblWishlist
    {
        public decimal Id { get; set; }
        public string UserId { get; set; }
        public decimal? ProductId { get; set; }
    }
}
