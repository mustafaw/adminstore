﻿using System;
using System.Collections.Generic;

namespace AdminStore.Models
{
    public partial class TblProductUnit
    {
        public TblProductUnit()
        {
            TblProduct = new HashSet<TblProduct>();
        }

        public decimal Id { get; set; }
        public string UnitName { get; set; }

        public virtual ICollection<TblProduct> TblProduct { get; set; }
    }
}
